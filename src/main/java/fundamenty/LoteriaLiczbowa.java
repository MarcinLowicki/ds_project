package fundamenty;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class LoteriaLiczbowa {

    public static void main(String[] args) {
        int[] wprowadzone = wprowadzod1do24();
        int[] losowane = wylosuj6liczb();

        int couterIleTrafien = porownaj( wprowadzone, losowane );
        wyswietlWynikNagrody( couterIleTrafien );

    }


    public static int[] wprowadzod1do24() {

        Scanner scanner = new Scanner( System.in );
        int[] przechowaj6cyfr = new int[6];
        int liczba;
        boolean niepoprawnaLiczba;

        for (int i = 0; i < przechowaj6cyfr.length; i++) {
            System.out.println( "Podaj liczbe z przedzialu od 1 do 24" );
            do {
                liczba = scanner.nextInt();
                niepoprawnaLiczba = (liczba < 1 || liczba > 24);
                if (niepoprawnaLiczba) {
                    System.out.println( "Podles liczbe spoza zakresu" );
                } else {
                    przechowaj6cyfr[i] = liczba;
                }
            } while (niepoprawnaLiczba);


        }
        System.out.println( Arrays.toString( przechowaj6cyfr ) );
        return przechowaj6cyfr;
    }

    public static int[] wylosuj6liczb() {

        Random randomCyfra = new Random();
        int[] przechowaj6losowanychCyfr = new int[6];
        int liczba;
        for (int i = 0; i < przechowaj6losowanychCyfr.length; i++) {
            //System.out.println( "LOSUJE liczbe z przedzialu od 1 do 24" );
            //losuje od 0 do 23 i plus 1 aby liczyl od 1 do 24.
            liczba = randomCyfra.nextInt( 24 ) + 1;
            przechowaj6losowanychCyfr[i] = liczba;
        }
        System.out.println( Arrays.toString( przechowaj6losowanychCyfr ) );
        return przechowaj6losowanychCyfr;
    }

    public static int porownaj(int[] x, int y[]) {
        int couter = 0;
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (x[i] == y[j]) {
                    couter++;
                    System.out.println( x[i] );
                }

            }


        }
        System.out.println( "Ile trafionych" + couter );
        return couter;
    }

    public static void wyswietlWynikNagrody(int z) {

        if (z >=0 && z <=2){
            System.out.println("Wygrales o zl");
        } else if(z ==3){
            System.out.println("Wygrales 16 zl");
        } else if(z ==4){
            System.out.println("wygrales 200 zl");
        } else if(z ==5){
            System.out.println("Wygrales 4000");
        } else {
            System.out.println("Wygrales 15000000");
        }


    }


}
