package fundamenty;

import org.w3c.dom.ls.LSOutput;

public class PetleWhile {

    public static void main(String[] args) {

        int i = 1;
        while (i <= 10) {
            System.out.println( i );
            i++;
        }

        String[] friends = {"Pele", "Neymar", "Tomek Badura"};
        for (int j = 0; j < friends.length; j++) {
            System.out.println( "Hej " + friends[j] );

        }

        String text = "Kasia lubi zakupy";
        System.out.println(text.contains( "dom" ));
        System.out.println(text.charAt( 3 ));
        System.out.println(text.indexOf( 2 ));
        System.out.println(text.substring( 6,10 ));
        System.out.println(text.substring( 0,text.indexOf( " " ) ));
    }




}
