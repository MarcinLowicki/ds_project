package fundamenty.sklepAGD.model;
public class Kredyt {

    private int iloscRat;
    private double cenaTowaru;


    public Kredyt(int iloscRat, double cenaTowaru) {
        this.iloscRat = iloscRat;
        this.cenaTowaru = cenaTowaru;
    }

    public boolean czyPoprawneDane() {
        return iloscRat >= 6 &&
                iloscRat <= 48 &&
                cenaTowaru >= 100 &&
                cenaTowaru <= 10000;
    }

    // 0.1
    // 0.05
    // 0.025

    //metoda zwraca -1 dla niepoprawnej ilosci rat, komunikujac w ten sposob bladne dane
    public double wyliczOprocentowanie() {
        if (!czyPoprawneDane()) {
            return -1;
        }

        if (iloscRat >= 6 && iloscRat <= 12) {
            return 0.025;
        } else if (iloscRat >= 13 && iloscRat <= 24) {
            return 0.05;
        } else {
            return 0.1;
        }

    }

    // wyliczCeneRaty
    public double wyliczCeneRaty() {
        return cenaTowaru / iloscRat;

    }


    //wyliczCeneRatyZOprocentowaniem
    public double wyliczCeneRatyZOprocentowaniem() {
        return wyliczCeneRaty() * wyliczOprocentowanie();
    }


    public String toString() {
        return "Ilosc rat wynosi: " + iloscRat +
                "\n Cena towaru wynosi: " + cenaTowaru;
    }

}