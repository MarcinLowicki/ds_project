package fundamenty.sklepAGD.service;

import fundamenty.sklepAGD.model.Kredyt;

import java.util.Scanner;

public class SklepAGD {

    public void przyjmijKlienta() {
        //zebrać dane klienta(cena ilosc rat)
        double cena = zapytajOliczbe( "Podaj cene: " );
        int raty = (int) zapytajOliczbe( "Podaj ilość RAT: " );

        Kredyt kredyt = new Kredyt( raty, cena );
        if (!kredyt.czyPoprawneDane()) {
            System.err.println( "Podales bledna cene lub ilosc rat" );

            przyjmijKlienta();
            return;
        }

        //dobrać kredyt (odpowiednie oprocentowanie)
        System.out.println( "Twoje oprocentowanie wynosi: " + kredyt.wyliczOprocentowanie() * 100 + " % - procent" );

        //wyswietlić informacje o kredycie (cena raty)
        System.out.println( kredyt );
        System.out.println("Cena raty bez oprocentowania wynosi: "+ kredyt.wyliczCeneRaty());
        System.out.println("Cena raty z oprocentowaniem "+ kredyt.wyliczOprocentowanie() *100 + "% wynosi: "+kredyt.wyliczCeneRatyZOprocentowaniem());


    }
    //wyswietl komunikat i pobierz wynik

    private double zapytajOliczbe(String pytanie) {
        System.out.println( pytanie );
        Scanner input = new Scanner( System.in );
        return input.nextDouble();
    }
}


//obiekty modelowe - reprezentuja rzeczy z cechami i zachowaniami
//serwisy - zwykle nie maja cech, wykonuja operacje na innych obiektach