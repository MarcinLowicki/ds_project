package fundamenty.programowanieWstep;

import java.util.Scanner;

public class Zmienne_05_do_08 {
    //5. Kalkulator
    int z1 = 20;
    int z2 = 5;
    int z3 = 12;
    int zSuma = z1 + z2 + z3;

    //6 . Zmienna ilość minut
    public void pobierzMinuty(float totalMinuty) {
        float totalGodziny1 =  totalMinuty / 60; //
        float totalGodziny = totalMinuty % 60;
        System.out.println( "Podano godzin " + totalGodziny1 + " oraz minut =" + totalGodziny );
    }

    // 7. Kalkulator walut
    public void obliczEuro(float kwota) {
        System.out.println( "Podaj kurs:" );
        Scanner scanner = new Scanner( System.in );
        float kurs = scanner.nextFloat();
        System.out.println( "Kurs =" + kurs + "kwota do obliczenia " + kwota );
        float obliczWartosc = kurs * kwota;
        System.out.println( obliczWartosc );
    }

    public void obliczaWskaznikBMI() {
        double waga;
        double wzrost;
        Scanner input = new Scanner( System.in );
        System.out.println( "Podaj wage w KG:" );
        waga = input.nextDouble();
        System.out.println( "Podaj wzrost w M:" );
        wzrost = input.nextDouble();

        double bmi = waga  / Math.pow( wzrost, wzrost );

        System.out.println( "BMI wynosi: " + bmi);

        if(bmi > 24.9){
            System.out.println("Nadwaga - wynik wynik wiekszy niz 24.9");
        } if (bmi < 18.5){
            System.out.println("Niedowaga - wynik mniejszy niz 18.5");
        }

    }


    public static void main(String[] args) {
        //5.
        Zmienne_05_do_08 zmienne = new Zmienne_05_do_08();
        System.out.println( "Podane liczby to: " + zmienne.z1 + " " + zmienne.z2 + " " + zmienne.z3 +
                " " + "a ich suma to=" + zmienne.zSuma );
        //6.
        zmienne.pobierzMinuty( 140 );
        //7.
        zmienne.obliczEuro( 100 );
        //8.
        zmienne.obliczaWskaznikBMI();

    }

}
