package fundamenty.programowanieWstep;

public class Wyswietlanie_1do3 {

    public static void main(String[] args) {

        // 1 Napisz program, który wyświetli napis “Witaj Świecie!”
        System.out.println( "Witaj Świecie!" );

        // 2 Napisz program, który wyświetli w konsoli:
        System.out.println( "Zaczynam naukę na kursie: “Java od Podstaw”\n" +
                "Pora więc na podstawy\n" );

        for (int i = 0; i < 4; i++) {
            if (i == 0) {
                System.out.println( "       J" );
            }
            if (i == 1) {
                System.out.print( "       A\n" );
            }
            if (i == 2) {
                System.out.print( "       V\n" );
            }
            if (i == 3) {
                System.out.print( "       A\n" );
            }
        }


        // 3 Wyświetl literały typów:
        int i = 10; //int
        double d = 20.22; //double
        String name = "string"; //String
        char charLetter = 'a'; //char
        boolean isTrue = true;
        Boolean isTrue2 = Boolean.valueOf( true ); //boolean//


    }
}

