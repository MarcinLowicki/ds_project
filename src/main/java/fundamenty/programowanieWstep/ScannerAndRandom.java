package fundamenty.programowanieWstep;

import java.util.Random;
import java.util.Scanner;

public class ScannerAndRandom {

    //9. *Napisz program który: pobierze imię, nazwisko i wiek,
    //następnie przywita użytkownika oraz poinformuje czy użytkownik jest pełnoletni.

/*
// 10.  Napisz program, który będzie generował losowe liczby, tak, jakbyś rzucał kostkami do gry.
 Napierw ma zapytać ile ścianek ma kostka, którą ma rzucić, a następnie zasymuluje rzut kostką o wybranej liczbie ścian
 i wyświetli wylosowaną liczbę.
* Wariant trudniejszy: pobierz od użytkownika jaka ma być najmniejsza i jaka największa wartość losowania a następnie wylosuj 5 liczb z zadanego zakresu.
*/

    public int rzucKostkaOilosciScian(int x) {
        Random r = new Random();
        return  r.nextInt( x )+1;
    }


    public static void main(String[] args) {

//9.
        Scanner input = new Scanner( System.in );
        System.out.println( " Podaj imie:" );
        String  imie = input.next();
        System.out.println( " Podaj nazwisko" );
        String nazwisko = input.next();
        System.out.println( " Podaj wiek" );
        int    wiek = input.nextInt();
        if (wiek > 18) {
            System.out.println( "Jestes pelnoletnim nastolatkiem" );
        }
        if (wiek < 18) {
            System.out.println( "Jestes dzieckiem" );
        }
        if (wiek > 100) {
            System.out.println( wiek + " to fajny wiek" );
        }

        //10.
        ScannerAndRandom scannerAndRandom = new ScannerAndRandom();



        int iluPrzeciwnikow =  scannerAndRandom.rzucKostkaOilosciScian( 6 );
        System.out.println( "przeciwnicy; " +  iluPrzeciwnikow);

        for (int i = 0; i < 5; i++) {
            System.out.println("wylosowano : " +  scannerAndRandom.rzucKostkaOilosciScian( 2 ) );
        }
    }
}


