package fundamenty.loteria;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class SymulatorLoterii {


    // todo: metoda DRY don't repeat your self and KISS keep it stupid simple


    public static void uruchomLoterie() {
        int ileMaBycLiczb = 3;

        int[] wylosowane6Liczb = losowanieLiczb( ileMaBycLiczb );
        System.out.println( Arrays.toString( wylosowane6Liczb ) );

        int[] wprowadzone6Liczb = wprowadzenieLiczb( ileMaBycLiczb );
        System.out.println( Arrays.toString( wprowadzone6Liczb ) );

        int iloscTrafien = sprawdzIleTrafiles( wylosowane6Liczb, wprowadzone6Liczb );

    }


    public static int[] losowanieLiczb(int ile) {

        Random random = new Random();
        int[] liczbaWtablicy = new int[ile];

        for (int i = 0; i < ile; i++) {

            liczbaWtablicy[i] = random.nextInt( 24 );
        }

        return liczbaWtablicy;
    }

    public static int[] wprowadzenieLiczb(int ile) {

        Scanner input = new Scanner( System.in );
        int[] tablicaLiczb = new int[ile];
        for (int i = 0; i < ile; i++) {
            int daneZinputu;
            do {
                System.out.println( "Podaj liczbe " + (i + 1) + " z zakresu 1 - 24" );

                daneZinputu = input.nextInt();

                tablicaLiczb[i] = daneZinputu;

            } while (daneZinputu <= 0 || daneZinputu >= 25);

        }

//todo metoda ma zwracac fundamenty.tablice liczb
// todo jesli liczba ze zlego zakresu pobieraj az bedzie dobra i dopiero idz do kolejnego miejsca

        return tablicaLiczb;
    }

    public static int sprawdzIleTrafiles(int[] tablicaRandom, int[] tablicaScanner) {
        int iloscTrafien =0;
        for (int i = 0; i <tablicaRandom.length; i++) {

            if(tablicaRandom[i]==tablicaScanner[i]){
                iloscTrafien++;

            }
        }
        //[5, 13, 21]
        //[5, 21, 1]

        return iloscTrafien;
    }


}
