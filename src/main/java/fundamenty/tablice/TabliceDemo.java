package fundamenty.tablice;

public class TabliceDemo {



    public static void main(String[] args) {
        double cena1 = 9.99;
        double cena2 = 15.99;
        double cena3 = 5.99;
        double cena4 = 3.99;


        System.out.println(cena1);
        System.out.println(cena2);
        System.out.println(cena3);
        System.out.println(cena4);

        if (cena1 > 5) {
            cena1 *= 1.1;
        }

        double[] ceny = new double[4];
        ceny[0] = 9.99;
        ceny[1] = 15.99;
        ceny[2] = 5.99;
        ceny[3] = 3.99;

        System.out.println("Jedna z cen: " + ceny[0]);
        System.out.println("Jedna z cen: " + ceny[1]);
        System.out.println("Jedna z cen: " + ceny[2]);
        System.out.println("Jedna z cen: " + ceny[3]);

        for(int i = 0 ; i<ceny.length;i++ ){
            if (ceny[i] > 5) {
                ceny[i] *= 1.1;
            }
            System.out.println( i );
            System.out.println("Jedna z cen: " + ceny[i]);
        }

        System.out.println(ceny.length);





    }
}
