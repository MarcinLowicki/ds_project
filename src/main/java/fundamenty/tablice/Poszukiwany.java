package fundamenty.tablice;

public class Poszukiwany {

    public static void main(String[] args) {

        int[] liczby = new int[4];
        String[] texty = new String[2];

        liczby[0] = 02;
        liczby[1] = 11;
        liczby[2] = 1984;
        liczby[3] = 100000;

        texty[0] = "Billy";
        texty[1] = "The Kid";

        for (int i = 0; i <4 ; i++) {
            System.out.println( liczby[i] );
            if (i < 2) {
                System.out.println( texty[i] );
            }


        }

        System.out.println("Wiadomość");
        System.out.println(liczby[0]+ " " + liczby[1] + " " + liczby[2] + "  zbiegł więzień " + texty[0]+ " " + texty[1] + " ");
        System.out.println(" Nagroda za przyprowadzenie " + liczby[3]);


        int binary = 0b101101;
        int octal = 020;
        int hexadecimal = 0x8a2b4f;
        System.out.println(binary);
        System.out.println(octal);
        System.out.println(hexadecimal);

    }
}
