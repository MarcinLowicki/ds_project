package obiektowe.dominik;

public class Auto {

    private String nazwaAuta;
    private String modelAuta;
    private String silnik;
    private int horsePower;
    private int iloscKol;

    public Auto(String nazwaAuta, String modelAuta, String silnik, int horsePower, int iloscKol) {
        this.nazwaAuta = nazwaAuta;
        this.modelAuta = modelAuta;
        this.silnik = silnik;
        this.horsePower = horsePower;
        this.iloscKol = iloscKol;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "nazwaAuta='" + nazwaAuta + '\'' +
                ", modelAuta='" + modelAuta + '\'' +
                ", silnik='" + silnik + '\'' +
                ", horsePower=" + horsePower +
                ", iloscKol=" + iloscKol +
                '}';
    }
}
