package obiektowe.enumy;

public enum Kierunek {

    POLNOC(0),
    WSCHOD(90),
    ZACHOD(270),
    POLUDNIE(180);

    private int stopnie;

   private Kierunek(int stopnie) {
        this.stopnie = stopnie;
    }

    public int getStopnie() {
        return stopnie;
    }
}
