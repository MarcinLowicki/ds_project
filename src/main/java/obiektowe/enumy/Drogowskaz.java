package obiektowe.enumy;

public class Drogowskaz {

    private Kierunek wKtoraStrone;

    public Drogowskaz(Kierunek wKtoraStrone) {
        this.wKtoraStrone = wKtoraStrone;
    }

    public Kierunek getwKtoraStrone() {
        return wKtoraStrone;
    }

    public void wyswietl(){

        System.out.println("Drogowskaz skierowany w strone " + wKtoraStrone + " i stopni ma " +  wKtoraStrone.getStopnie());

    }


    @Override
    public String toString() {
        return "Drogowskaz{" +
                "wKtoraStrone='" + wKtoraStrone + '\'' +
                '}';
    }
}
