package obiektowe.enumy;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        Drogowskaz pierwszy = new Drogowskaz(Kierunek.POLNOC);
        Drogowskaz drugi = new Drogowskaz( Kierunek.ZACHOD);
        Drogowskaz trzeci = new Drogowskaz(Kierunek.POLUDNIE);
       // Kierunek czwarty = new Kierunek(50);

        pierwszy.wyswietl();
        drugi.wyswietl();
        trzeci.wyswietl();

        switch (trzeci.getwKtoraStrone()){

            case POLNOC:
                System.out.println("Idziesz prosto");
                break;

            case ZACHOD:
                System.out.println("Idziesz w lewo");
                break;

            case WSCHOD:
                System.out.println("Idziesz w prawo");
                break;

            case POLUDNIE:
                System.out.println("Idziesz w dol");
                break;

            default:
                System.out.println("Stoisz w miejscu juz jakis czas");
        }


        Kierunek[] values = Kierunek.values();

        for (Kierunek kierunek : values) {
            System.out.println(kierunek +  " " +  kierunek.getStopnie());
        }

        System.out.println(pierwszy);
        System.out.println(drugi);

/*

        JFrame frame = new JFrame(  );

        frame.setVisible( true );
        frame.setSize( 500,500 );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
*/

    }
}
