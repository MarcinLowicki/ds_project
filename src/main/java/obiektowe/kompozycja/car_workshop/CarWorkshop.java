package obiektowe.kompozycja.car_workshop;

public class CarWorkshop {

    private double bill;
    private double priceForService=20;


    void repairCar(Car car) {
        int howMuchRepair=0;
        for (Wheel wheel : car.getWheels()) {

            if (wheel.isCracked()) {
                howMuchRepair++;
                wheel.repair();
            }
            bill=howMuchRepair*priceForService;
        }
        System.out.println("Ilosc kol naprawionych " + howMuchRepair +
                " koszt sumaryczny = " +bill);
    }

}
