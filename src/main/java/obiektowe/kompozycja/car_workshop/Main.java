package obiektowe.kompozycja.car_workshop;

public class Main {

    public static void main(String[] args) {

        if (false) {
            System.out.println( "Hello" );
        } else {
            System.out.println( "no hello" );
        }

        Car car = new Car();
        CarWorkshop carWorkshop = new CarWorkshop();
        System.out.println( car );

        car.crackRandomWheel();
        car.crackRandomWheel();
        System.out.println( car );

        carWorkshop.repairCar( car );

        System.out.println( "**************" );
        System.out.println( car );


        Car car1 = new Car();
        car1.crackRandomWheel();
        carWorkshop.repairCar( car1 );
        System.out.println( car1 );


    }
}
