package obiektowe.kompozycja.car_workshop;

public class Wheel {

    private boolean cracked;
    private double presure = 2.00;

    public boolean isCracked() {
        return cracked;
    }

    void crack(){
        this.cracked=true;
        this.presure=0.00;
    }

    void repair(){
        this.cracked=false;
        this.presure=2.00;
    }




    @Override
    public String toString() {
        return "Wheel{" +
                "presure=" + presure +
                ", cracked=" + cracked +
                '}';
    }
}
