package obiektowe.kompozycja.car_workshop;

import java.util.Arrays;
import java.util.Random;

public class Car {

    private Wheel[] wheels;

    public Car() {
        Wheel[] wheels = new Wheel[4];
        this.wheels = wheels;
        for (int i = 0; i < wheels.length; i++) {
            wheels[i] = new Wheel();
        }
    }

    public Wheel[] getWheels() {
        return wheels;
    }

    void crackRandomWheel() {
        Random random = new Random();
        int randomWheel = random.nextInt( 4 );
        this.wheels[randomWheel].crack();
    }


    @Override
    public String toString() {
        return "Car{" +
                "wheels=" + Arrays.toString( wheels ) +
                '}';
    }
}
