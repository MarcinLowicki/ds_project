package obiektowe.kompozycja.garden;

public class Main {

    public static void main(String[] args) {


        Bench bench = new Bench();
        System.out.println( bench );

        Tree tree = new Tree();
        System.out.println( tree );

        Dog dog = new Dog( "Reksio" );
        Dog dog1 = new Dog( );
        System.out.println( dog );
        Flower[] flowers = {new Flower( Color.BIALY ), new Flower( Color.ZOLTY ), new Flower()};


        Garden garden = new Garden( flowers, 2 );

        System.out.println( garden );

        Bench bench1 = new Bench( "plastik" );
        System.out.println( bench1 );

        Flower flower = new Flower();
        flower.waterTheFlower();
        System.out.println( flower );

        dog.dogPlay();
        dog1.dogPlay();
        garden.goInsideTheGarden();
        garden.watheringTheFlouers();
        System.out.println(garden);
        garden.rest();
        garden.collectFruits();
        System.out.println(garden);
        garden.collectFruits();
        garden.exitGarden();
        System.out.println(garden);

        System.out.println("----------------");
        garden.workInTheGarden();
        System.out.println(garden);

        System.out.println("************************");

        Gardener gardener = new Gardener();
        gardener.oneMonthWork( garden );
        System.out.println(garden);

    }
}
