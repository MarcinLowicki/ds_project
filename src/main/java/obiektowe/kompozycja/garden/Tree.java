package obiektowe.kompozycja.garden;

import java.util.Random;

public class Tree {

    private int numerOfFruits;

    public Tree() {
        Random random = new Random();
        numerOfFruits = random.nextInt( 901 ) + 100;
    }

    public int getNumerOfFruits() {
        return numerOfFruits;
    }

    void givesFruits() {
        Random random = new Random();
        numerOfFruits += random.nextInt( 100 ) + 1;
    }

    int collectFruit() {
        int firstNumberOfFruits = numerOfFruits;
        this.numerOfFruits = 0;
        return firstNumberOfFruits;
    }


    @Override
    public String toString() {
        return "Tree{" +
                "numerOfFruits=" + numerOfFruits +
                '}';
    }
}
