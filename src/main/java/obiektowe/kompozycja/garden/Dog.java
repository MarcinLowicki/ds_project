package obiektowe.kompozycja.garden;

import java.util.Random;

public class Dog {

    private String name;

    private String[] tricks = {"aport", "bierz", "daj lape", "hau hau"};

    public Dog(String name) {

        this.name = name;
    }

    public Dog() {
        this( "Burek" );

    }

    void dogPlay() {
        System.out.println( name );
        Random random = new Random();
        int trickNumber = random.nextInt(tricks.length);
        System.out.println(tricks[trickNumber]);


    }


    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                '}';
    }
}
