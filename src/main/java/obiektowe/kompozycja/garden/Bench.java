package obiektowe.kompozycja.garden;

import java.util.Random;

public class Bench {

    static String[] materialPool = {"steel", "wood", "plastik"};
    private String material;



    public Bench() {
        Random random = new Random();
        int materialIndex = random.nextInt( materialPool.length );
        material = materialPool[materialIndex];
    }

    public Bench(String materialName) {
        for (String s : materialPool) {
            if (s.equals( materialName )) {
                System.out.println( "Twoj material to " + s );
                this.material = materialName;
                return;
            }
        }
        System.out.println( "Niewlasciwy material" );
    }

    public String getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Bench{" +
                "material='" + material + '\'' +
                '}';
    }
}
