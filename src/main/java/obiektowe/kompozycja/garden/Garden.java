package obiektowe.kompozycja.garden;

import java.util.Arrays;
import java.util.Random;

public class Garden {

    private Bench bench;
    private Dog dog;
    private Flower[] flowers;
    private Tree[] trees;

    public Garden(Flower[] flowers, int numerOfTrees, Dog dog) {
        this.flowers = flowers;
        Bench bench1 = new Bench();
        this.bench = bench1;
        trees = new Tree[numerOfTrees];

        for (int i = 0; i < trees.length; i++) {
            trees[i] = new Tree();
        }
        this.dog = dog;
    }

    public Garden(Flower[] flowers, int numerOfTrees) {
        this( flowers, numerOfTrees, null );
    }

    void goInsideTheGarden() {
        if (dog != null) {
            dog.dogPlay();
        } else {
            System.out.println( "No dog in the Garden" );
        }
    }

    void watheringTheFlouers() {
        for (Flower flower : flowers) {
            flower.waterTheFlower();
        }
    }

    void rest() {
        System.out.println( "Siadasz na lawce z materialu " + bench.getMaterial() );
    }

    int collectFruits() {
        int numerOfFruits = 0;
        for (Tree tree : trees) {
            numerOfFruits += tree.collectFruit();
        }
        if (numerOfFruits == 0) {
            System.out.println( "Przyjdz jutro" );
        } else {
            System.out.println( "Laczna suma owocow " + numerOfFruits );
        }
        return numerOfFruits;
    }

    void exitGarden() {
        for (Tree tree : trees) {
            tree.givesFruits();
        }
        for (Flower flower : flowers) {
            flower.setBlooms( false );
        }
    }

    int workInTheGarden() {
        goInsideTheGarden();
        watheringTheFlouers();
        rest();
        int number = collectFruits();
        exitGarden();
        return number;
    }


    @Override
    public String toString() {
        return "Garden{" +
                "bench=" + bench +
                ", dog=" + dog +
                ", flowers=" + Arrays.toString( flowers ) +
                ", trees=" + Arrays.toString( trees ) +
                '}';
    }
}
