package obiektowe.kompozycja.garden;

import java.util.Random;

public class Flower {

    private Color color;
    private boolean blooms = false;

    public Flower(Color color) {
        this.color = color;
    }

    public Flower() {
        Random random = new Random();
        int wylosowany = random.nextInt( Color.values().length );
        color = Color.values()[wylosowany];
    }

    public boolean isBlooms() {
        return blooms;
    }

    void setBlooms( boolean blooms){
        this.blooms=blooms;
    }

    public void waterTheFlower() {
        blooms = true;
    }


    @Override
    public String toString() {
        return "Flower{" +
                "color=" + color +
                ", blooms=" + blooms +
                '}';
    }
}
