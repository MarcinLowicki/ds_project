package obiektowe.kompozycja.house;

import java.util.Arrays;

public class Room {

    private Bed bed;
    private Window[] okna;

    public Room(Bed bed, int iloscOkien) {
        this.bed = bed;
        okna = new Window[iloscOkien];
        for (int i = 0; i < iloscOkien; i++) {
            okna[i] = new Window();
        }

    }

    void posprzataj(){
        bed.posprzatajLozko( true );
        for (Window window : okna) {
            window.zmienStanOkna( true );
        }

    }


    @Override
    public String toString() {
        return "Room{" +
                "bed=" + bed +
                ", okna=" + Arrays.toString( okna ) +
                '}';
    }
}
