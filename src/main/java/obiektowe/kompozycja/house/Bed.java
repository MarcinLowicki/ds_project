package obiektowe.kompozycja.house;

public class Bed {

    private boolean czyLozkoJestPoscielone;
    private String kolorPoscieli;


    public Bed(String kolorPoscieli){
        this.kolorPoscieli=kolorPoscieli;
    }

    void posprzatajLozko(boolean czyLozkoJestPoscielone){
        this.czyLozkoJestPoscielone=czyLozkoJestPoscielone;
    }


    @Override
    public String toString() {
        return "Bed{" +
                "czyLozkoJestPoscielone=" + czyLozkoJestPoscielone +
                ", kolorPoscieli='" + kolorPoscieli + '\'' +
                '}';
    }
}
