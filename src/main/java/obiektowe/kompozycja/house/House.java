package obiektowe.kompozycja.house;

import java.util.Arrays;

public class House {

    private Door drzwi;
    private Room[] pokoje;

    public House(Room[] pokoje){
        drzwi = new Door();
        this.pokoje=pokoje;
    }

    void posprzatajHouse(){
        for (Room room : pokoje) {
            room.posprzataj();
        }
    }

/*    void wyswietlDom(){
        System.out.println("Drzwi: " + drzwi);
        System.out.println("Pokoje: " + Arrays.toString( pokoje ) );
    }*/

    @Override
    public String toString() {
        return "House{" +
                "drzwi=" + drzwi +
                ", pokoje=" + Arrays.toString( pokoje ) +
                '}';
    }
}
