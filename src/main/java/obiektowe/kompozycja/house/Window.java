package obiektowe.kompozycja.house;

public class Window {

    private boolean oknoOtwarteCzyNie;

    void zmienStanOkna(boolean polecenie){
        oknoOtwarteCzyNie=polecenie;

    }

    void setOknoOtwarteCzyNie(boolean oknoOtwarteCzyNie) {
        this.oknoOtwarteCzyNie = oknoOtwarteCzyNie;

    }

    @Override
    public String toString() {
        return "Window{" +
                "oknoOtwarteCzyNie=" + oknoOtwarteCzyNie +
                '}';
    }
}
