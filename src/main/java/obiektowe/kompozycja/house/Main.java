package obiektowe.kompozycja.house;

public class Main {

    public static void main(String[] args) {
        Room[] pokoj1 = new Room[2];
        pokoj1[0] = new Room( new Bed( "kolorowy" ), 2 );
        pokoj1[1] = new Room( new Bed( "biala" ), 3 );

        Room[] pokoj2 = new Room[2];
        pokoj2[0] = new Room( new Bed( "rozowa" ),2 );
        pokoj2[1] = new Room( new Bed( "niebieska" ),3 );

        House house1 = new House( pokoj1 );
        House house2 = new House( pokoj2 );
        System.out.println( house1 );
        System.out.println( house2 );

        house2.posprzatajHouse();
        System.out.println( house1 );
        System.out.println( house2 );
    }
}
