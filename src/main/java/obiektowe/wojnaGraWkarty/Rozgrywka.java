package obiektowe.wojnaGraWkarty;

import java.util.ArrayList;
import java.util.List;

public class Rozgrywka {

    private Gracz gracz1;
    private Gracz gracz2;
    private TaliaKart taliaKart;
    private List<Karta> kartyZagrywane = new ArrayList<>();


    void zagrajTurniej() {

        gracz1 = new Gracz( "Pierwszy Gracz" );
        gracz2 = new Gracz( "Drugi Gracz" );
        taliaKart = new TaliaKart();

        taliaKart.rozdajKarty( gracz1, gracz2 );
        do {
            runda();
            kartyZagrywane.clear();
        } while (!gracz1.czySkonczylyCiSieKarty() && !gracz2.czySkonczylyCiSieKarty());
        System.out.println( "Koniec!" );
    }


    public void runda() {
        Karta zagranaKartaGracza1 = gracz1.giveKarte();
        System.out.println( "Gracz1:" + zagranaKartaGracza1 );
        Karta zagranaKartaGracza2 = gracz2.giveKarte();
        System.out.println( "Gracz2:" + zagranaKartaGracza2 );

        kartyZagrywane.add( zagranaKartaGracza1 );
        kartyZagrywane.add( zagranaKartaGracza2 );

        if (zagranaKartaGracza1.silaKarty() == zagranaKartaGracza2.silaKarty()) {
            System.out.println( "!!!WOJNA!!!" );
            if (gracz1.czySkonczylyCiSieKarty() || gracz2.czySkonczylyCiSieKarty()){
                System.out.println("Miala byc wojna ale skonczyly sie karty");
                return;
            }
            runda();

            // TODO: naprawic blad ze znikajacymi kartami w wojnie
            //TODO: Dziedziczenie


        } else if (zagranaKartaGracza1.silaKarty() > zagranaKartaGracza2.silaKarty()) {
            gracz1.addWygraneWieleKarte( kartyZagrywane );
            System.out.println( "Gracz1 zgarnia karty: " + kartyZagrywane );

        } else if (zagranaKartaGracza1.silaKarty() < zagranaKartaGracza2.silaKarty()) {
            gracz2.addWygraneWieleKarte( kartyZagrywane );
            System.out.println( "Gracz2 zgarnia karty: " + kartyZagrywane );
        }

        System.out.println( gracz1 );
        System.out.println( gracz2 );
        System.out.println( gracz1.ileMaszKart() );
        System.out.println( gracz2.ileMaszKart() );


    }


    @Override
    public String toString() {
        return "Rozgrywka{" +
                "gracz1=" + gracz1 +
                ", gracz2=" + gracz2 +
                ", taliaKart=" + taliaKart +
                '}';
    }
}
