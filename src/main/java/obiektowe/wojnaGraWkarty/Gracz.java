package obiektowe.wojnaGraWkarty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Gracz {

    private List<Karta> odliczoneKarty;
    private List<Karta> wygraneKarty;
    private String name;

    public Gracz(String name) {
        this.name = name;
        //Obiekt (pole) musi zostac zainicjalizowane inaczej trafia tam Null.
        odliczoneKarty = new ArrayList<>();
        wygraneKarty = new ArrayList<>();
    }

    public int ileMaszKart(){
        int sumaKart =0;
        sumaKart=odliczoneKarty.size()+wygraneKarty.size();

        return sumaKart;
    }

    public void addStartowaKarta(Karta karta) {
        odliczoneKarty.add( karta );
    }

    public void addWygranaKarte(Karta karta) {
        //Null Pointer Exception wystepuje gdy wywolujemy metode na zmiennej obiektowej w ktorej jest null
        System.out.println( wygraneKarty );
        wygraneKarty.add( karta );
    }

    public void addWygraneWieleKarte(List<Karta> karty) {
        //Null Pointer Exception wystepuje gdy wywolujemy metode na zmiennej obiektowej w ktorej jest null
        System.out.println( wygraneKarty );
        wygraneKarty.addAll( karty );
    }

    public Karta giveKarte() {
        if (odliczoneKarty.isEmpty()) {
            przygotujNowaTalieKart();
        }
        Karta zagranaKarta = odliczoneKarty.get( 0 );
        odliczoneKarty.remove( 0 );
        return zagranaKarta;
    }

    public void przygotujNowaTalieKart() {
        odliczoneKarty.addAll( wygraneKarty );
        Collections.shuffle( odliczoneKarty );
        wygraneKarty.clear();
    }

    public boolean czySkonczylyCiSieKarty() {
        return odliczoneKarty.isEmpty() && wygraneKarty.isEmpty();
    }

    @Override
    public String toString() {
        return "Gracz{" +
                "odliczoneKarty=" + odliczoneKarty +
                ", wygraneKarty=" + wygraneKarty +
                ", name='" + name + '\'' +
                '}';
    }
}
