package obiektowe.wojnaGraWkarty;

public enum Waga {

    DZIEWIEC(9), DZIESIEC(10), JUPEK(11), DAMA(12), KROL(13), AS(14);
   private int sila;

    Waga(int sila) {
        this.sila = sila;
    }

    public int getSila() {
        return sila;
    }
}
