package obiektowe.wojnaGraWkarty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaliaKart {

    private List<Karta> taliaKart;

    public TaliaKart() {
        taliaKart = new ArrayList<>();
        for (Kolor kolor : Kolor.values()) {

            for (Waga waga : Waga.values()) {
                taliaKart.add( new Karta( kolor, waga ) );

            }
        }
        Collections.shuffle( taliaKart );

    }


    void rozdajKarty(Gracz gracz1, Gracz gracz2) {

        for (int i = 0; i < taliaKart.size(); i++) {
            if (i % 2 != 0) {
                gracz1.addStartowaKarta( taliaKart.get( i ) );
            } else {
                gracz2.addStartowaKarta( taliaKart.get( i ) );
            }

        }

    }


    @Override
    public String toString() {
        return "TaliaKart{" +
                "taliaKart=" + taliaKart +
                '}';
    }
}
