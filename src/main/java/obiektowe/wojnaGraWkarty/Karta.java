package obiektowe.wojnaGraWkarty;


public class Karta {

    private Kolor kolor;
    private Waga waga;

    public Karta(Kolor kolor, Waga waga) {
        this.kolor = kolor;
        this.waga = waga;
    }

    int silaKarty() {
        return waga.getSila();
    }

    @Override
    public String toString() {
        return waga.name().toLowerCase() + " sila: "+ silaKarty()+" " + kolor.name().toLowerCase();
    }
}
