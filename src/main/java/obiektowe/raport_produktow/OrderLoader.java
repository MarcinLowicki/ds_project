package obiektowe.raport_produktow;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class OrderLoader {


    public List<CarOrder> loadOrder(String filePath) {

        List<CarOrder> carOrdersList = new ArrayList<>();
        File file = new File( filePath );
        try {
            Scanner scanner = new Scanner( file );
            while (scanner.hasNextLine()) {
                String csvLineInput = scanner.nextLine();
                CarOrder carOrder = createOrder( csvLineInput );
                carOrdersList.add( carOrder );
            }
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            System.out.println( "File not foud" );
        }
        // System.out.println(carOrdersList);
        return carOrdersList;
    }


    private CarOrder createOrder(String rowFromFile) {

        // System.out.println( csvLineInput );
        String[] tabl = rowFromFile.split( "," );
        //System.out.println( Arrays.toString( tabl ) );
        String carShop = tabl[0];
        double carPrice = Double.parseDouble( tabl[3] );
        Currency currency = Currency.valueOf( tabl[4].toUpperCase() );
        LocalDate data = LocalDate.parse( tabl[5] );
        Car car = new Car( tabl[1], tabl[2] );
        // System.out.println( carShop + " " + carPrice + " " + currency + " " + data + " " + car );
        //  TODO: parsowane dane wstawic do CarODer . Car order wstawic do listy,
//             dodac petle , ktora dpowtorzy to do kazdej nastepnej linijki.
        //CarOrder carOrder = new CarOrder();
        CarOrder carOrder = new CarOrder( carShop, carPrice, currency, data, car );

        return carOrder;
    }


}
