package obiektowe.raport_produktow;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void main(String[] args) {

        OrderLoader orderLoader = new OrderLoader();

        List<CarOrder> carOrderList = orderLoader.loadOrder(
                "src\\main\\java\\obiektowe\\raport_produktow\\products.csv" );

        System.out.println( carOrderList );
        System.out.println( "**********************" );
        GeneratorCarOrderReport generatorCarOrderReport = new GeneratorCarOrderReport( carOrderList );
        double sumAll = 0;
        sumAll = generatorCarOrderReport.summaryCarSales();
        System.out.println( sumAll );
        double sumByBrand = generatorCarOrderReport.summaryCarSalesByBrand( "Bmw" );
        System.out.println( sumByBrand );

        carOrderList = generatorCarOrderReport.generateRaportFromDay( LocalDate.parse( "2021-10-01" ));
        for (CarOrder carOrder : carOrderList) {
            System.out.println(carOrder);
        }

        System.out.println("*******RANGE DATE: *****");
        carOrderList = generatorCarOrderReport.generateRaportFromDateRange( LocalDate.parse( "2018-12-01"), LocalDate.parse( "2021-12-01") );
        for (CarOrder carOrder : carOrderList) {
            System.out.println(carOrder);
        }
    }
}
