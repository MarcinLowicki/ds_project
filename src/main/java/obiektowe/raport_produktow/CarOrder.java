package obiektowe.raport_produktow;

import java.time.LocalDate;

public class CarOrder {

    private String carShop;
    private double carPrice;
    private Currency currency;
    private LocalDate data;
    private Car car;

    public CarOrder(String carShop, double carPrice, Currency currency, LocalDate data, Car car) {
        this.carShop = carShop;
        this.carPrice = carPrice;
        this.currency = currency;
        this.data = data;
        this.car = car;
    }

    public double getCarPrice() {
        return carPrice;
    }

    public Car getCar() {
        return car;
    }

    public LocalDate getData() {
        return data;
    }



    @Override
    public String toString() {
        return "CarOrder{" +
                "carShop='" + carShop + '\'' +
                ", carPrice=" + carPrice +
                ", currency=" + currency +
                ", data=" + data +
                ", car=" + car +
                '}';
    }
}
