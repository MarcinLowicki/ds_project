package obiektowe.raport_produktow;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GeneratorCarOrderReport {

    private List<CarOrder> orders;

    public GeneratorCarOrderReport(List<CarOrder> orders) {
        this.orders = orders;
    }

    public double summaryCarSales() {
        double sum = 0;
        for (CarOrder order : orders) {
            sum += order.getCarPrice();
        }
        return sum;
    }

    public double summaryCarSalesByBrand(String brandCar) {
        double sum = 0;
        for (CarOrder order : orders) {
            if (brandCar.equals( order.getCar().getMarka() )) {
                sum += order.getCarPrice();
            } else {
                //System.out.println( "Brak Marki" );
            }
        }
        return sum;
    }

    public List<CarOrder> generateRaportFromDay(LocalDate date) {
        List<CarOrder> newOrderList = new ArrayList<>();
        for (CarOrder order : orders) {
            if (date.equals( order.getData() )) {
                newOrderList.add( order );
            }
        }
        return newOrderList;
    }

    public List<CarOrder> generateRaportFromDateRange(LocalDate beginDate1, LocalDate endDate2) {
        List<CarOrder> carOrderDateRange = new ArrayList<>();
        for (CarOrder carOrder : orders) {
            if ((carOrder.getData().isAfter( beginDate1 ) || carOrder.getData().isEqual( beginDate1 ))
                    &&
                    (carOrder.getData().isBefore( endDate2 ) || carOrder.getData().isEqual( endDate2 ))
            ) {
                carOrderDateRange.add( carOrder );
            }

        }
        return carOrderDateRange;
    }

    public List<CarOrder> carsBoughtInCurrency(Currency waluta) {
        // przegladanie calej listy orderow, wyciaganie do nowej listy tych gdzie jest np PLN(currnacy) i zwracanie nowej listy

        return null;
    }

}


