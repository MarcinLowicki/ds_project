package obiektowe.inheritance;


public class SoccerPLayer extends Worker implements PracownikFizyczny, PracownikWsektPrywatnym{


    private League currentLeague;

    public SoccerPLayer(String imie, double zarobekPodstawowy, League currentLeague) {
        //super - przy dziedziczeniu nalezy najwpier wywolac konstrukto z korej dziedziczymy poprzez super
        super( imie, zarobekPodstawowy );
        this.currentLeague = currentLeague;
    }


    public SoccerPLayer() {
        super( "Robert", 100 );
        currentLeague = League.A;
    }

    public void naJakiejUmowiePracujesz() {
        System.out.println( "Contract" );

    }

    public double ileZarabiasz() {

        switch (currentLeague) {
            case C:
                return zarobekPodstawowy * 0.5;
            case B:
                return zarobekPodstawowy * 0.8;
            case A:
                return zarobekPodstawowy;
            case OKREGOWKA:
                return zarobekPodstawowy * 1.5;
            case LIGA5:
                return zarobekPodstawowy * 10;
            case LIGA4:
                return zarobekPodstawowy * 15;
            case LIGA3:
                return zarobekPodstawowy * 50;
            case LIGA2:
                return zarobekPodstawowy * 100;
            case LIGA1:
                return zarobekPodstawowy * 150;
            case EKSTRAKLAPA:
                return zarobekPodstawowy * 10_000;
            default:
                System.out.println( "Default" );
                return zarobekPodstawowy;
        }


    }

/*
    public void jakaMaszUmowe() {
        System.out.println( "Contract" );
    }
*/

    public boolean czyGraszWeSport() {
        if (ileZarabiasz() < 10000) {
            System.out.println( "TAK gram" );
            return true;
        } else {
            System.out.println( "NIE gram bo sie nagralem :)" );
        }

        return true;
    }

    @Override
    public String toString() {
        return "SoccerPLayer{" +
                "imie='" + imie + '\'' +
                ", zarobekPodstawowy=" + zarobekPodstawowy +
                ", currentLeague=" + currentLeague +
                '}';
    }

    @Override
    public void zalozKorki() {

        System.out.println("Korki OK");

    }

    @Override
    public void pracujPonadMiare() {

        System.out.println(" Be like Robert i trenuj");

    }
}
