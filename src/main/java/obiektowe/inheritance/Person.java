package obiektowe.inheritance;

public class Person {

    private String imie;

    public Person(String imie) {
        this.imie = imie;
    }

    public Person() {

    }

    public static void main(String[] args) {
        Person person = new Person();
    }
}
