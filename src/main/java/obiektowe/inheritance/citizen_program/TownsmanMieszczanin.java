package obiektowe.inheritance.citizen_program;

public class TownsmanMieszczanin extends Citizen {


    public TownsmanMieszczanin(String imie) {
        super( imie );
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
