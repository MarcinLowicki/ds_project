package obiektowe.inheritance.citizen_program;

import java.util.ArrayList;
import java.util.List;

public class TownMiasto {

    private List<Citizen> citizenLisst;

    public TownMiasto() {
        citizenLisst = new ArrayList<>();
    }

    public void addCitizen(Citizen citizen) {
        citizenLisst.add( citizen );
    }

    public int howManyCanVote() {
        int x = 0;
        for (Citizen citizen : citizenLisst) {
            if (citizen.canVote() == true) {
                x++;
            }
        }
        return x;
    }

    public void whoCanVote() {
        for (Citizen citizen : citizenLisst) {
            if (citizen.canVote() == true) {
                System.out.println(citizen.getImie());
            }
        }
    }

    @Override
    public String toString() {
        return "TownMiasto{" +
                "citizenLisst=" + citizenLisst +
                '}';
    }
}
