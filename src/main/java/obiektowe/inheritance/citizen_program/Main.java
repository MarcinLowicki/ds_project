package obiektowe.inheritance.citizen_program;

public class Main {


    public static void main(String[] args) {

        TownMiasto townMiasto = new TownMiasto();
        Citizen kingKrol = new KingKrol("Krol Artur");
        Citizen peasantChlop = new PeasantChlop("Chlopi");
        Citizen townsmanMieszczanin = new TownsmanMieszczanin("DJ");
        Citizen soldierZolnierz = new SoldierZolnierz("WarMan");


        townMiasto.addCitizen( kingKrol );
        townMiasto.addCitizen( peasantChlop );
        townMiasto.addCitizen( townsmanMieszczanin );
        townMiasto.addCitizen( soldierZolnierz );

        System.out.println("MIASTO ma:");
        System.out.println(townMiasto);


        System.out.println( "Can vote: " );
        int y = townMiasto.howManyCanVote();
        System.out.println(y);

        System.out.println("Kto moze glosowac:");

        townMiasto.whoCanVote();
    }
}
