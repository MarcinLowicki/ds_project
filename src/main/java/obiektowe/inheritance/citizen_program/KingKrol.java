package obiektowe.inheritance.citizen_program;

public class KingKrol extends Citizen {


    public KingKrol(String imie) {
        super( imie );
    }

    @Override
    public boolean canVote() {
        return false;
    }
}
