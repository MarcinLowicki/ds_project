package obiektowe.inheritance.citizen_program;

public abstract class Citizen {

    private String imie;

    public Citizen(String imie) {
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public abstract boolean canVote();

    @Override
    public String toString() {
        return "Citizen{" +
                "imie='" + imie + '\'' +
                '}';
    }
}
