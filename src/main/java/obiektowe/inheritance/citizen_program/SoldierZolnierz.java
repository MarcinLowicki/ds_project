package obiektowe.inheritance.citizen_program;

public class SoldierZolnierz extends Citizen {


    public SoldierZolnierz(String imie) {
        super( imie );
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
