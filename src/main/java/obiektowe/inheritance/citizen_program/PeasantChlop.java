package obiektowe.inheritance.citizen_program;

public class PeasantChlop extends Citizen {


    public PeasantChlop(String imie) {
        super( imie );
    }

    @Override
    public boolean canVote() {
        return false;
    }
}
