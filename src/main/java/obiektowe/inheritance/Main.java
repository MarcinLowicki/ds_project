package obiektowe.inheritance;

import java.util.ArrayList;
import java.util.List;

public class Main {
//TODO: Dalej dziedziczenie konstruktory i pola i interfejsy i wyjatki 8 i 10.11

    public static void main(String[] args) {
        Programmer programmer1 = new Programmer();
        Worker programmer2 = new Programmer( "Marcin", 4000 );


        programmer2
                .setZarobekPodstawowy( 9000 )
                .setImie( "Karol" );


// TODO: Dziedziczenie, abstrakcyjne klasy, polimorfizm


        System.out.println(programmer1);
        System.out.println(programmer2);


        boolean czyOgladasz = programmer1.czyOgladaszStackOverflow();
        double ileZarabiasz = programmer1.ileZarabiasz();
        programmer1.naJakiejUmowiePracujesz();

        System.out.println(czyOgladasz + "  " + ileZarabiasz);

        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

        Worker soccerPLayer1 = new SoccerPLayer();
        // Typ zmiennej (Worker) decyduje o tym jakie metody beda widoczne i jakie obiekty bedzie mozna do niej wstawiac
        // new ScoccerPLayer() rodzaj stworzonego obiektu decyduje ktore wersje metod beda uruchamiane
        SoccerPLayer soccerPLayer2 = new SoccerPLayer("Daniel",1000000, League.LIGA1);
        System.out.println(soccerPLayer1);
        System.out.println(soccerPLayer2);
        Rekruter rekruter = new Rekruter();

        //rekruter.przeprowadzRozmoweZprogramista( programmer1 );
        rekruter.przeprowadzRozmowe( programmer1 );
        rekruter.przeprowadzRozmowe( soccerPLayer1 );
       // rekruter.przeprowadzRozmoweZpilkarzem( soccerPLayer1 );
       // nie mozna zrobic obiektu z abstrakcyjnej klasy Worker worker1 = new Worker();

        List<Worker> pracownicy = new ArrayList<>(  );

        pracownicy.add(programmer1 );
        pracownicy.add(programmer2 );
        pracownicy.add(soccerPLayer1 );
        pracownicy.add(soccerPLayer2 );
       // pracownicy.add(worker1 );
        pracownicy.add(new Menager("Daniel", 20000));

        for (Worker worker : pracownicy) {

            worker.naJakiejUmowiePracujesz();
        }

        Przedsiebiorstwo przedsiebiorstwo = new Przedsiebiorstwo();
        Polityk polityk = new Polityk( "Jaro",50_000 );
        przedsiebiorstwo.zatrudnijPracownika( programmer1 );
        przedsiebiorstwo.zatrudnijPracownika( soccerPLayer2 );
        // Metoda zatrudnij przyjmuje tylko tych ktorych implementuja wymagany interfejs
        //przedsiebiorstwo.zatrudnijPracownika( polityk );

    }

}
