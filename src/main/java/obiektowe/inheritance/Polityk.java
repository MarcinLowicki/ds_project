package obiektowe.inheritance;

public class Polityk extends Worker implements PracownikWsektPublicznym, PracownikFizyczny {

    public Polityk(String imie, double zarobekPodstawowy) {
        super( imie, zarobekPodstawowy );
    }

    @Override
    public void zalozKorki() {
        System.out.println(" Polityk gra w przerwie");
    }

    @Override
    public void obijajSie() {
        System.out.println(" Polityk lubi sie obijac");
    }

    @Override
    public double ileZarabiasz() {

        System.out.println("Duzo zarabiam bo czy sie stoi czy sie lezy dieta sie nalezy");
        return 20_000;
    }
}
