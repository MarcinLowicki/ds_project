package obiektowe.inheritance;

public abstract class  Worker  {

    // private - widoczne tylko w klasie
    //  - default, gdy nic nie napisze - dostep w ramach pakietu
    // protected - dla pakietu i klas dziedziczacych
    // public - we wszystkich pakietach

    protected String imie;
    protected double zarobekPodstawowy;

    public Worker(String imie, double zarobekPodstawowy) {
        this.imie = imie;
        this.zarobekPodstawowy = zarobekPodstawowy;
    }

    // seter przyjmuje to co musze edytowac
    public Worker setZarobekPodstawowy(double zarobekPodstawowy) {
        this.zarobekPodstawowy = zarobekPodstawowy;
        return this;
    }


    public Worker setImie(String imie) {
        this.imie = imie;
        return this;
    }

    public void naJakiejUmowiePracujesz(){
        System.out.println("Nie twoj interes :)");
    }
        // nie abstract metody nie musze byc nadpisywane ale moge.

    public abstract double ileZarabiasz();
// metode abstrakcyjna robimy gdy nie ma sensu jakiegos domyslnego dzialania a chcemy miec metode wspolna dla
// wszysstkich klas  dziedziczacych. Musze je nadpisywac.


    // Animal - makeSound()-abstrakcyjna dla Dog i Cat. lub eat()
}
