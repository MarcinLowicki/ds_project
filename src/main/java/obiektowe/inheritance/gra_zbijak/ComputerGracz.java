package obiektowe.inheritance.gra_zbijak;

import java.util.Random;

public class ComputerGracz extends Gracz {


    // metoda szablonowa - template method (behawioral)
    private int iloscRuchow = 0;

    public ComputerGracz(String nazwa, int wspolrzednaX, int wspolrzednaY) {
        super( nazwa, wspolrzednaX, wspolrzednaY );
    }

    @Override
    public void ruch() {
        //iloscRuchow = iloscRuchow + 1;
        iloscRuchow++;
        if (iloscRuchow % 2 == 0) {
            return;
        }
        super.ruch();
    }

    @Override
    public String wybierzRuch() {
        System.out.println( "Ruch komputera" );
        Random random = new Random();

        int kierunek = random.nextInt( 4 );
        String[] letters = {"N","S","W","E"};
        return letters[kierunek];
    }

    @Override
    public void cofnijRuch(int poprzedniaPozycjaX, int poprzedniaPozycjaY) {
        iloscRuchow--;
        super.cofnijRuch( poprzedniaPozycjaX, poprzedniaPozycjaY );
       //cofa pozycje i robi ruch jeszcze raz

        // iloscRuchow = iloscRuchow - 1;
    }
}
