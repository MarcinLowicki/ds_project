package obiektowe.inheritance.gra_zbijak;

import java.util.Scanner;

public class HumanGracz extends Gracz {
    public HumanGracz(String nazwa, int wspolrzednaX, int wspolrzednaY) {
        super( nazwa, wspolrzednaX, wspolrzednaY );
    }

    @Override
    public String wybierzRuch() {

        Scanner scanner = new Scanner( System.in );
        System.out.println( "Podaj wspolrzedna ruchu( N,S,W,E: )" );
        String ruch = scanner.next();
        return ruch;
    }

}
