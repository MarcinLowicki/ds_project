package obiektowe.inheritance.gra_zbijak;

public abstract class Gracz {

    private String nazwa;
    protected int wspolrzednaX;
    protected int wspolrzednaY;

    public Gracz(String nazwa, int wspolrzednaX, int wspolrzednaY) {
        this.nazwa = nazwa;
        this.wspolrzednaX = wspolrzednaX;
        this.wspolrzednaY = wspolrzednaY;
    }

    public void ruch() {

        String ruch = wybierzRuch();
        System.out.println( wspolrzednaX + " " + wspolrzednaY );
        int poprzedniaPozycjaX = wspolrzednaX;
        int poprzedniaPozycjaY = wspolrzednaY;
        poruszGracza( ruch );

        if (czyNiePoprawnyRuch()) {
            cofnijRuch( poprzedniaPozycjaX, poprzedniaPozycjaY );
        }
    }
    public abstract String wybierzRuch();

    public void poruszGracza(String ruch) {

        switch (ruch) {
            case "N":
                //System.out.println( " wGora" );
                wspolrzednaY = wspolrzednaY - 1;
                break;
            case "S":
                //System.out.println( "w Dol" );
                wspolrzednaY = wspolrzednaY + 1;
                break;
            case "W":
                //System.out.println( "wLewo" );
                wspolrzednaX = wspolrzednaX - 1;
                break;
            case "E":
                //System.out.println( "w Prawo" );
                wspolrzednaX = wspolrzednaX + 1;
                break;
            default:
                System.out.println( "Mozesz podac tylko N,S,W lub E" );
                //
        }
    }

    public boolean czyNiePoprawnyRuch() {

        return (wspolrzednaX <= -1 || wspolrzednaX > 9) || (wspolrzednaY <= -1 || wspolrzednaY > 9);
    }

    public void cofnijRuch(int poprzedniaPozycjaX, int poprzedniaPozycjaY) {
        System.out.println( "Bledne koordynaty" );
        wspolrzednaX = poprzedniaPozycjaX;
        wspolrzednaY = poprzedniaPozycjaY;
        ruch();
    }

    public int getWspolrzedneX() {
        return wspolrzednaX;
    }

    public int getWspolrzedneY() {
        return wspolrzednaY;
    }

    public String getNazwa() {
        return nazwa;
    }

    @Override
    public String toString() {
        return "Gracz{" +
                "nazwa='" + nazwa + '\'' +
                ", wspolrzednaX=" + wspolrzednaX +
                ", wspolrzednaY=" + wspolrzednaY +
                '}';
    }
}
