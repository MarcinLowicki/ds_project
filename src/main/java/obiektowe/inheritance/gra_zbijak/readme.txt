ZBIJAK
Zaimplementuj prostą grę w zbijanego.
Gracz wpisując odpowiednio kierunki góra, dół, lewo, prawo
(‘G’, ‘D’, ‘L‘, ‘P’) ma za zadanie poruszać pionkiem po planszy o wymiarach 10 x 10.
Na planszy ma się znajdować 3 innych graczy komputerów, którzy co dwa ruchy gracza wykonują swoje ruchy w
losowym kierunku.
Zadaniem gracza jest zbicie wszystkich przeciwników. Do zbicia dochodzi w
momencie gdy pionek gracza znajduje się na tym samym polu co przeciwnik.

Gra kończy się wypisanie ilości ruchów gracza w momencie gdy wszystkie ofiary zostają zbite z planszy lub
komputer zbije gracza.

W projekcie należy zastosować dziedziczenie oraz abstrakcyjną klasę.