package obiektowe.inheritance.gra_zbijak;

import java.util.List;

public class Gra {

    private Plansza plansza;
    private List<Gracz> gracze;
    private Gracz graczHuman;


    public Gra() {
        plansza = new Plansza();
        graczHuman= new HumanGracz( "MA", 9, 9 );
        plansza.addGraczy( graczHuman);
        plansza.addGraczy( new ComputerGracz( "C1", 0, 9 ) );
        plansza.addGraczy( new ComputerGracz( "C2", 9, 0 ) );
        plansza.addGraczy( new ComputerGracz( "C3", 0, 0 ) );
        /* plansza.getListaGraczy();*/
        gracze = plansza.getListaGraczy();
    }

    public void repeatGame() {
        while (gracze.size()>1 && gracze.contains( graczHuman )) {
            zagrajWgre();
        }
        System.out.println(" Koniec gry: ");
        plansza.showPlansza();
    }

    public void zagrajWgre() {
        plansza.showPlansza();
        System.out.println( plansza );

        for (int i = 0; i < gracze.size(); i++) {
            Gracz gracz = gracze.get( i );
            gracz.ruch();
            usunGraczaPrzyZbiciu( gracz );
        }
    }
    public void usunGraczaPrzyZbiciu(Gracz graczRobiacyRuch) {
        for (int i = 0; i < gracze.size(); i++) {
            Gracz graczPrzegladany = gracze.get( i );
            if (graczPrzegladany.getWspolrzedneX() == graczRobiacyRuch.getWspolrzedneX()
                    &&
                    graczPrzegladany.getWspolrzedneY() == graczRobiacyRuch.getWspolrzedneY() && (graczPrzegladany !=graczRobiacyRuch)) {

                System.out.println( graczPrzegladany );
                gracze.remove( graczPrzegladany );

                //TODO: wyciagniecie graczy z listy(koordynaty) - porownanie ich i usuniecie z listy.
                //
                //TODO: Interfejsy i wyjatki iprojekty.


            }

        }
    }
}
