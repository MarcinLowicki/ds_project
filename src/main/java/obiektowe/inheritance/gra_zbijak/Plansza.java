package obiektowe.inheritance.gra_zbijak;

import java.util.ArrayList;
import java.util.List;

public class Plansza {

    private List<Gracz> listaGraczy = new ArrayList<>();

    public List<Gracz> getListaGraczy() {
        return listaGraczy;
    }
    public void addGraczy(Gracz gracz) {
        listaGraczy.add( gracz );
    }

    public void showPlansza() {
        System.out.println();
        for (int pionY = 0; pionY < 10; pionY++) {
            for (int poziomX = 0; poziomX < 10; poziomX++) {
                int x, y;


                //wrzocicdo jednej petli i pokazac innych graczy.
                // Nastepne rzecz to ruszanie graczami.
                boolean czyBylGracz = false;
                for (int k = 0; k < listaGraczy.size(); k++) {
                    Gracz gracz = listaGraczy.get( k );
                    x = gracz.getWspolrzedneX();
                    y = gracz.getWspolrzedneY();
                    if (pionY == y && poziomX == x) {
                        System.out.print( gracz.getNazwa()+ " " );
                        czyBylGracz = true;
                    }
                }
                if (!czyBylGracz) {
                    //Tu decyduje czy drukuje '0' czy nie.
                    System.out.print( " 0 " );
                }

            }
            System.out.println();
        }
    }

    @Override
    public String toString() {
        return "Plansza{" +
                "listaGraczy=" + listaGraczy +
                '}';
    }
}
