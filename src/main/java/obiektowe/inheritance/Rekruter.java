package obiektowe.inheritance;


public class Rekruter {

    public void przeprowadzRozmowe(Worker worker) {

        System.out.println( "Na jakiej umowie pracujesz?" );
        worker.naJakiejUmowiePracujesz();

        System.out.println( "Ile Zarabiasz?" );
        double d = worker.ileZarabiasz();
        System.out.println( d );

    }


}
