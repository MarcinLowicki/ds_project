package obiektowe.inheritance;

public class Programmer extends Worker implements PracownikUmyslowy, PracownikWsektPrywatnym {


    public Programmer(String imie, double zarobekPodstawowy) {
        super( imie, zarobekPodstawowy );
    }

    public Programmer() {
        super("Jan", 5000);
    }



    public void naJakiejUmowiePracujesz() {
        System.out.println("B2B");
    }


    public double ileZarabiasz() {
        return zarobekPodstawowy * 2;
    }
/*
    public void jakaMaszUmowe() {
        System.out.println( "B2B" );
    }*/

    public boolean czyOgladaszStackOverflow() {
        if (ileZarabiasz() < 5000) {
            System.out.println( "TAK bo zarabiasz mniej niz 5k" );
            return true;
        } else {
            System.out.println( "NIE bo zarabiasz wiecej niz 5k" );

        }

        return true;
    }

    @Override
    public String toString() {
        return "Programmer{" +
                "imie='" + imie + '\'' +
                ", zarobekPodstawowy=" + zarobekPodstawowy +
                '}';
    }

    @Override
    public void zalaczKomputer() {
        System.out.println("PC załączony");
    }

    @Override
    public void pracujPonadMiare() {

        System.out.println("Masz za malo nadgodzin");
    }
}
