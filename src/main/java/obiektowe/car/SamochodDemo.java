package obiektowe.car;

public class SamochodDemo {


    public static void main(String[] args) {

        Samochod[] tablicaSamochodow = new Samochod[2];

        System.out.println( "*****************************" );
        Samochod samochod1 = new Samochod( "Tesla", 5000 );

        samochod1.wyswietl();
        samochod1.jedzOdleglosc( 3000 );
        samochod1.wyswietl();
        //samochod1.ileDoPrzegladu();
        System.out.println( "*****************************" );
        Samochod samochod2 = new Samochod( "VW", 5000 );

        samochod2.wyswietl();
        samochod2.jedzOdleglosc( 3000 );
        samochod2.wyswietl();
        // samochod2.ileDoPrzegladu();

        tablicaSamochodow[0] = samochod1;
        tablicaSamochodow[1] = samochod2;

        tablicaSamochodow[0].wyswietl();
        tablicaSamochodow[1].wyswietl();

        for (Samochod samochod : tablicaSamochodow) {
            if (samochod.getPrzebieg() >= 5000.00) {
                System.out.println("Powyzej 10 000=");
                samochod.wyswietl();
            }
        }
    samochod1.setMarka( "BMW" );
        samochod1.wyswietl();
    }



}
