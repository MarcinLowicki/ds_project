package obiektowe.car;

public class Samochod {

/*
    Stwórz klasę typu Samochod oraz SamochodDemo z metodą main.
    W metodzie main stwórz dwa obiekty typu samochód.
    Obiekty typu Samochod mają mieć następujące cechy: marka, przebieg oraz przebieg do przeglądu,
    ta ostatnia uzupełniona jakąś wartością np 20 000 km.
    W metodzie main nadaj wartości dla cech stworzonych wcześniej samochodów.

    Dla obiektów typu Samochod przygotuj następujące zachowania oraz przetestuj je w main:

Stwórz metodę wyświetlającą markę samochodu oraz przebieg - OK
Stwórz metodę przyjmującą odległość do przejechania i zwiększającą przebieg samochodu o tę odległość
Stwórz metodę zwracającą ilość kilometrów po których trzeba będzie wykonać przegląd
(uwzględniając aktualny przebieg)

wariant trudniejszy: stwórz w main tablicę samochodów a następnie wykorzystując pętlę wyświetl dane każdego z nich

*/

    private String marka;
    private double przebieg;
    private double zaIlePrzeglad = 20000.00;

    public Samochod(String marka, double przebieg){
        this.marka=marka;
        this.przebieg=przebieg;
    }

    public double getPrzebieg() {
        return przebieg;
    }

    public Samochod setMarka(String marka) {
        this.marka = marka;
        return this;
    }

    void wyswietl(){
        System.out.println(" Marka " + marka);
        System.out.println(" Przebieg " + przebieg);
        System.out.println( "Przeglad za " + ileDoPrzegladu() + " km");

    }

    double ileDoPrzegladu(){
        double odliczajKm = zaIlePrzeglad-przebieg;
    return odliczajKm;
    }

    void jedzOdleglosc(double ileKmJedziesz){
        if (ileKmJedziesz <= 0){
            System.err.println("Ty oszuscie");
        }else {
            przebieg = przebieg + ileKmJedziesz;
        }

    }



}
