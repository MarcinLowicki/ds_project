package obiektowe.linia;

public class LiniaDemo {

    public static void main(String[] args) {

        Linia linia1 = new Linia(3, "#");
        Linia linia2 = new Linia(5, "@");
        Linia linia3 = new Linia(8, "%");

        System.out.println(linia1);
        System.out.println(linia1.toString());

/*        linia1.dlugiscWmm = 3;
        linia1.wypelnienieSymbolem = "#";
        linia1.drukujLinie();
        System.out.println();

        linia2.dlugiscWmm = 5;
        linia2.wypelnienieSymbolem = "@";
        linia2.drukujLinie();
        System.out.println();
        linia3.dlugiscWmm = 8;
        linia3.wypelnienieSymbolem = "%";
        linia3.drukujLinie();
        System.out.println();*/

        linia1.drukujLinie();
        linia2.drukujLinie();
        linia3.drukujLinie();

    }


}
