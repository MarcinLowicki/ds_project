
Stwórz obiekt typu Karta (Card). Kartę będą opisywać dwa pola klas enumowych Rank(Ranga np. dziewiątka, as)
oraz Suit (Kolor - np. pik).
Stwórz dwie dowolne karty w mainie, różniące się rangą i kolorem,
zaprezentuj je wykorzystując nadpisaną metodę toString.
W mainie wyświetl wszystkie możliwe rangi wykorzystując statyczną metodę enumów - values().
Każda z rang ma mieć przypisaną siłę, dla dwójki 2, trójki 3 itd.
Obiekty kart mają mieć metodę pozwalającą na pobranie siłyKarty().
Stwórz dodatkowy serwis pozwalający porównać dwie karty, drukujący tą która jest większa.
//wyswietl sile pierwszej karty ____
Stwórz listę wszystkich możliwych kart w kolorze pik. (wykorzystaj pętle dla automatyzacji zadania)
Stwórz całą talię, czyli listę wszystkich możliwych kart dla wszystkich możliwych kolorów i rang.

Dla ambitnych - jako dodatkowe zadanie domowe możesz wykorzystać stworzony kod do
zaprogramowania prostej gry karcianej takiej jak Wojna lub trudniejszej takiej jak Poker czy Makao.
Jeśli zdecydujesz się na Pokera, zacznij od stworzenia klasy serwisowej analizatora, który pozwoli Ci sprawdzić, czy dla danej ręki pięciu kart występuje któraś z pokerowych kombinacji np. para, dwie pary itd.
