package obiektowe.kartyEnum;

public enum Waga {

    DZIEWIEC( 9 ),
    DZIESIEC( 10 ),
    JUPEK( 2 ),
    DAMA( 3 ),
    KROL( 4 ),
    AS( 11 );

    private int sila;

    Waga(int sila) {
        this.sila = sila;
    }

    public int getSila() {
        return sila;
    }
}
