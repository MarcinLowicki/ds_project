package obiektowe.kartyEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KartaDemo {

    public static void main(String[] args) {

        Karta kartaPierwsza = new Karta( Kolor.KARO, Waga.DZIESIEC );
        Karta kartaDruga = new Karta( Kolor.SERCE, Waga.KROL );
        List<Karta> listKart = new ArrayList<>();
        //listKart.add( new Karta( Kolor.PIK, Waga.DZIESIEC ) );
        System.out.println( "Lista Kart ! " + listKart );

        System.out.println( "Sila karty: " + kartaPierwsza.silaKarty() );
        Wojna wojna = new Wojna();
        wojna.porownajKtoraKartaWygra( kartaPierwsza, kartaDruga );
        // dokonczyc w pliku
        Waga[] wagaValues = Waga.values();
        Kolor[] kolorsAll = Kolor.values();

 /*       for (Waga wagaValue : wagaValues) {
            listKart.add( new Karta( Kolor.PIK, wagaValue ) );
        }*/


        for (Kolor kolor : kolorsAll) {
            for (Waga wagaValue : wagaValues) {
                listKart.add( new Karta( kolor, wagaValue ) );
            }

        }
        Collections.shuffle( listKart );

        System.out.println( listKart );
    }



}


