package obiektowe.mlTrial;

import java.util.Scanner;

public class NewPage {


    public int openNextPage2(int currentPage) {

        Scanner scanner = new Scanner( System.in );
        System.out.println( "Open next page? " + " Give Y or N" );
        String x = scanner.nextLine();
        if (x.toUpperCase().equals( "Y" )) {
            currentPage++;
            System.out.println( "Has open next page" );
            return openNextPage2( currentPage );
        } else {
            System.out.println( " Back to Main " + currentPage );
            return currentPage;
        }

    }

}
