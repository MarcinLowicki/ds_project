package obiektowe.mlTrial;

import java.util.Scanner;

public class Rekurencja2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner( System.in );
        System.out.print( "Podaj potęgę:" );
        int potega = scanner.nextInt();
        System.out.println( "Dla liczby 2 wynosi:" );

        int x = liczbaPodniesionaDoPotegi( potega );
        System.out.println( x );

        System.out.println( "REKURENCJA:" );

        int y = rekurencyjnieLiczbaPodniesionaDoPotegi( 4 );
        System.out.println( y );
    }


    public static int liczbaPodniesionaDoPotegi(int potega) {
        int wynik = 1;
        for (int i = 0; i < potega; i++) {
            wynik = 2 * wynik;
        }
        return wynik;

    }

    public static int rekurencyjnieLiczbaPodniesionaDoPotegi(int potega) {
        if (potega == 0) {
            return 1;
        } else {
            --potega;
            return rekurencyjnieLiczbaPodniesionaDoPotegi( potega ) * 2;
        }
    }

        public static int poteguj(int potega){
            if (potega == 0) return 1;
            return poteguj( --potega ) * 2;
        }



}
