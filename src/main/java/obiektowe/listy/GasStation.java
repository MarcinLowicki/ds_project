package obiektowe.listy;

import java.util.List;

public class GasStation {

private String name;
private List<Fuel> fuels;
private double price;
private String adres;

    public GasStation(String name, List<Fuel> fuels, double price, String adres) {
        this.name = name;
        this.fuels = fuels;
        this.price = price;
        this.adres = adres;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
}
