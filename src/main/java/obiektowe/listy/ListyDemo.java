package obiektowe.listy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListyDemo {

    public static void main(String[] args) {

        int[] tablicaLiczb = {1, 5, 4, 7, 9};

        //metoda przyjmuje tablice i liczbe (2 arg) i tworzy o 1 wieksza tablice i dodaje do niej te liczbe na ostatnie miejsce. Na koniec zwraca nowa tablice


        int[] nowaTurboTablica = dodajLiczbeDoTablicy( tablicaLiczb, 11 );
        System.out.println( Arrays.toString( nowaTurboTablica ) );

        int[] nowaTurboTablica2 = dodajLiczbeDoTablicy( nowaTurboTablica, 30 );
        System.out.println( Arrays.toString( nowaTurboTablica2 ) );
// cechy listy, zmienny rozmiar, gotowe metody. Lista jest jedna z kolekcji. Inne to Map, Set, Kolejki(Queue), Stosy(Deque)
        List<Integer> numbers = new ArrayList<>();
        System.out.println( numbers );
        numbers.add( 99 );
        numbers.add( 13 );
        numbers.add( 50 );
        System.out.println( numbers );
        numbers.add( 17 );
        numbers.add( 15 );
        System.out.println( numbers + " argumentow:  " + numbers.size() );
        numbers.remove( 0 );

        System.out.println(numbers);

        int filed ;

        filed=numbers.get( 1 );
        System.out.println(filed);

        List<Integer> newList = new ArrayList<>( numbers );
        numbers.add( 88 );
        newList.add( 77 );
       System.out.println(newList);
       /*  newList.removeAll( numbers );
        System.out.println(newList);*/
        newList.retainAll( numbers );
        System.out.println(newList);

    }


    static int[] dodajLiczbeDoTablicy(int[] tablicaLiczb, int nowaliczba) {

        int[] nowaTablica = new int[tablicaLiczb.length + 1];
        nowaTablica[tablicaLiczb.length] = nowaliczba;
        for (int i = 0; i < tablicaLiczb.length; i++) {
            nowaTablica[i] = tablicaLiczb[i];
        }
        return nowaTablica;

    }
}



