package proceduralne.metody;

public class ReturnStatements {
    public static void main(String[] args) {
        printAMessage();
        int sum = add( 5, 5 );
        System.out.println( "To jest suma:" + sum );
        String duzeWyrazy = caps( "duze wyrazy napisz" );
        System.out.println( duzeWyrazy );

        int[] newArray = intArray( 0, 9, 8, 6 );
        System.out.println("Dlugosc newArray: " + newArray.length);
        System.out.println(newArray[0]);
        System.out.println(newArray[1]);
        System.out.println(newArray[2]);
        System.out.println(newArray[3]);

    }

    public static void printAMessage() {
        System.out.println( "To pierwsza metoda" );
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static String caps(String s) {
        return s.toUpperCase();
    }

    public static int[] intArray(int a, int b, int c, int d) {
        int[] array = new int[4];
        array[0] = a;
        array[1] = b;
        array[2] = c;
        array[3] = d;
        return array;
    }

}


