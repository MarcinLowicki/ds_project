package proceduralne.metody;

public class ReturnMethods {

    public static void main(String[] args) {

        int value = cube( 2 );
        System.out.println( value );

    }

    public static int cube(int numberToCube){
        System.out.println("Gora: ");
        return numberToCube * numberToCube * numberToCube;
    }
}
