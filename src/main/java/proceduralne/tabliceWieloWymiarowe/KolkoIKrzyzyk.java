package proceduralne.tabliceWieloWymiarowe;

import java.util.Scanner;

public class KolkoIKrzyzyk {

    // dwoch graczy, na zmiane wybieraja gdzie stawiaja znaczek O lub X . Plansza 3x3 z mozliwoscia zwiekszenia.
// WSTEP PROGRAMOWANIA OBJEKTOWEGO
    static final String PUSTE_POLE = "~", CIRCLE = "0", CROSS = "X";
    static String[][] plansza = {{PUSTE_POLE, PUSTE_POLE, PUSTE_POLE},
            {PUSTE_POLE, PUSTE_POLE, PUSTE_POLE},
            {PUSTE_POLE, PUSTE_POLE, PUSTE_POLE}};

    public static void main(String[] args) {
        System.out.println( "Gracz 1 wstaw znaczek na planszy" );
        zagraj();
    }

    static void zagraj() {
        do {

            if (czyKoniecGry()) {
                //kod co przerwie gre
                System.out.println( "KONIEC GRY" );
                break;
            }
            wyswietlPlansze( (plansza) );
            zagrajRunde( "X" );
            wyswietlPlansze( (plansza) );

            if (czyKoniecGry()) {
                //kod co przerwie gre
                System.out.println( "KONIEC GRY" );
                break;
            }
            zagrajRunde( "0" );
        } while (true);
    }

    static boolean czyKoniecGry() {
        return !czySaWolneMiejsca() ||
                !ktoWstawilPoziomeLinie().equals( PUSTE_POLE ) ||
                !ktoWstawilPionoweLinie().equals( PUSTE_POLE ) ||
                !ktoWstawilSkosZlewejDoPrawej().equals( PUSTE_POLE ) ||
                !ktoWstawilSkosZprawejDoLewej().equals( PUSTE_POLE );
    }

    static void wyswietlPlansze(String[][] plansza) {
        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza.length; j++) {
                System.out.print( plansza[i][j] + " " );
            }
            System.out.println( " " );
        }
    }

    static void wstawGracza(String symbol0LubX, int r, int k) {
        plansza[r][k] = symbol0LubX;
    }

    static void zagrajRunde(String symbol0LubX) {
        System.out.println( "Wybierz w ktore miejsce wskawiasz symbol" );
        Scanner scanner = new Scanner( System.in );
        System.out.println( "Wybierz wiersz" );
        int r = scanner.nextInt();
        System.out.println( "Wybierz kolumne" );
        int k = scanner.nextInt();
        if (czyPoprawnyRuch( r, k )) {
            wstawGracza( symbol0LubX, r, k );
        } else {
            System.out.println( "***Te wspolrzedne sa zajete, podaj poprawne***" );
            zagrajRunde( symbol0LubX );
        }
    }

    static boolean czyPoprawnyRuch(int r, int k) {
        if ((r >= 0 && r <= 2) && (k == 0 || k == 1 || k == 2)) {
            if (plansza[r][k].equals( PUSTE_POLE )) {
                return true;
            }
        }
        return false;
    }

    static boolean czySaWolneMiejsca() {
        for (int i = 0; i < plansza.length; i++) {
            for (int j = 0; j < plansza.length; j++) {
                if (plansza[i][j].equals( PUSTE_POLE )) {
                    return true;
                }
            }
        }
        return false;
    }

/*    static String ktoWygral() {
        return ktoWstawilPoziomeLinie();

    }*/

    static String ktoWstawilPoziomeLinie() {
        String zmiennaDoPorownania;
        for (int i = 0; i < plansza.length; i++) {
            zmiennaDoPorownania = plansza[i][0];
            int seria = 0;
            for (int j = 0; j < plansza.length; j++) {
                if (zmiennaDoPorownania.equals( plansza[i][j] )) {
                    seria++;
                 /*   System.out.println( "Ile jest pod rzad do SERII. ZNAK " + zmiennaDoPorownania +
                            " " + " SERIA: " + seria );*/
                    if (seria == 3 && !PUSTE_POLE.equals( zmiennaDoPorownania )) {
                        return zmiennaDoPorownania;
                    }
                }
            }
        }
        return PUSTE_POLE;
    }

    static String ktoWstawilPionoweLinie() {
        String zmiennaDoPorownania;
        for (int i = 0; i < plansza.length; i++) {
            zmiennaDoPorownania = plansza[i][0];
            int seria = 0;
            for (int j = 0; j < plansza.length; j++) {
            /*    System.out.println("i: " + i);
                System.out.println("j: " + j);*/
                if (zmiennaDoPorownania.equals( plansza[j][i] )) {
                    seria++;
                }
                if (seria == 3 && !zmiennaDoPorownania.equals( PUSTE_POLE )) {
                    return zmiennaDoPorownania;
                }
            }
        }
        return PUSTE_POLE;
    }

    static String ktoWstawilSkosZlewejDoPrawej() {
        String znak = plansza[0][0];
        int seria = 0;
        for (int i = 0; i < plansza.length; i++) {
            if (znak.equals( plansza[i][i] )) {
                seria++;
            }
        }
        if (seria == 3) {
            return znak;
        } else {
            return PUSTE_POLE;
        }

        /*String znakKtoryWygral;
        if (plansza[0][0].equals( plansza[1][1] ) && (plansza[2][2]).equals( plansza[0][0] )) {
            znakKtoryWygral = plansza[0][0];
            return znakKtoryWygral;
        } else {
            return PUSTE_POLE;
        }*/
    }

    static String ktoWstawilSkosZprawejDoLewej() {
        String znakKtorWygral;

        if (plansza[0][2].equals( plansza[1][1] ) && plansza[2][0].equals( plansza[0][2] )) {
            znakKtorWygral = plansza[0][2];
            return znakKtorWygral;
        } else {
            return PUSTE_POLE;
        }
    }

     /*for (int i = 0, j = 3; i < plansza.length; i++,j--) {
        if (znak.equals( plansza[i][j] )) {
            seria++;
        }
    }
*/

}
