package proceduralne.tabliceWieloWymiarowe;

import java.util.Random;
import java.util.Scanner;

public class TabliceWW {

    public static void main(String[] args) {
        int[][] x = new int[4][5];
        //int[][] x = stworzTabiceLosowychLiczb();
        x[0][0] = 9;
        x[3][4] = 8;
        x[1][3] = 5;
        x[2][1] = 7;
        x[3][2] = 3;

        // j wskazuje linie , wiersz (rzedy) w dół idzie
        for (int wiersz = 0; wiersz < x.length; wiersz++) {
            // i wskazuje na kolumne w prawo idzie
            for (int kolumna = 0; kolumna < x[1].length; kolumna++) {
                System.out.print( x[wiersz][kolumna] + " " );
            }
            System.out.println( " " );
        }
        for (int kolumna = 0; kolumna < x[2].length; kolumna++) {
            System.out.print( x[2][kolumna] + " " );
        }
        System.out.println( " " );
        System.out.println( " Wersja dla for each " );
        //tablica tablic x ( dla kazdego elementu o nazwie wiersz w zbiorze x
        for (int[] wiersz : x) {
            // a tu z tablic wyciagam po koleji jedna liczbe
            for (int liczbaZkolumny : wiersz) {
                System.out.print( liczbaZkolumny + " " );
            }
            System.out.println( " " );
        }
        drukujeTabliceWielowymiarowa( x );
        drukujWierszZTablicy( x, 3 );
        int wynikSumy;   //jezeli chce wiecej niz raz wykorzystac wynik z metody to wstawiam do zmiennej lub dla czytelnosci.
        System.out.println( sumujeLiczbyZtabicyWieowymiarowej( x ) ); // jezeli wykorzystuej raz to moge wstawic bezposrednio tam gdie jest zwrocona
        uzupelnijKolumneLiczba( x, 4, 4 );
        drukujeTabliceWielowymiarowa( x );
        uzupelnijPoPrzekatnejTablice(x,1);
        System.out.println("--------");
        drukujeTabliceWielowymiarowa( x );
    }
// static int [][] drukujeTabliceWielowymiarowa() - metoda tworzy i zwraca tablice return. Odbieram dane to uzywam typu, miejca przed nazwa.
// static void drukujeTabliceWielowymiarowa(int [][] nazwaTab) - metoda przyjmuje tablice i na niej pracuje . Wysylam dane do tej metody
//jeeli jest typ void - nie ma zwroconej wartosci. Metoda jest sposobem zeby wywolac serie instrukcji
// static String drukujeTabliceWielowymiarowa(String x, String y) - metoda przyjmuje dwa teksty i zwraca return jeden tekst


    static void drukujeTabliceWielowymiarowa(int[][] nazwaTab) {
        //for each = dla kazdego elementu ze zbioru x
        // for(deklaracja zmiennej: zbior)
        // przyklad dla tablicy intow: // for(int zmienna: nazwaTab)
        for (int[] wiersz : nazwaTab) {
            // a tu z tablic wyciagam po koleji jedna liczbe
            for (int liczbaZkolumny : wiersz) {
                System.out.print( liczbaZkolumny + " " );
            }
            System.out.println( " " );
        }
    }

    static void drukujWierszZTablicy(int[][] tablica, int wiersz) {
        for (int kolumna = 0; kolumna < tablica[wiersz].length; kolumna++) {
            System.out.print( tablica[wiersz][kolumna] + " " );
        }
        System.out.println( " " );
    }
    //metoda co sumuje wszystkie liczby z tablicy wielomymiarowej i zwraca sume

    static int sumujeLiczbyZtabicyWieowymiarowej(int[][] tablica) {
        int suma = 0;
        for (int wiersz = 0; wiersz < tablica.length; wiersz++) {
            // i wskazuje na kolumne w prawo idzie
            for (int kolumna = 0; kolumna < tablica[1].length; kolumna++) {
                suma += tablica[wiersz][kolumna];
            }
        }
        return suma;
    }

    static int[][] stworzTabiceLosowychLiczb() {

        Scanner podajLiczbe = new Scanner( System.in );
        int i = podajLiczbe.nextInt();
        int j = podajLiczbe.nextInt();

        int[][] tablicaiorazJ = new int[i][j];
        Random randomoweLiczby = new Random();
        for (int wiersz = 0; wiersz < tablicaiorazJ.length; wiersz++) {
            // i wskazuje na kolumne w prawo idzie
            for (int kolumna = 0; kolumna < tablicaiorazJ[1].length; kolumna++) {
                tablicaiorazJ[wiersz][kolumna] = randomoweLiczby.nextInt( 25 );
            }
        }
        return tablicaiorazJ;
    }
    //metoda przyjmje tablice oraz liczbe a takze numer kolumny, uzupelnia te kolumne przekazana liczba
    // 3 argumenty i nic nie zwraca

    static void uzupelnijKolumneLiczba(int[][] tablica1, int kolumna1, int liczba1) {
        for (int i = 0; i < tablica1.length; i++) {
           tablica1[i][kolumna1] = liczba1;
        }
    }
// metoda przyjmuje tablice i liczbe i wstawia liczbe na przekatnej tablicy

    static void uzupelnijPoPrzekatnejTablice(int[][] tablica1, int liczba) {

        for (int i = 0, x=0; i < tablica1.length; i++, x++) {
            tablica1[i][x]=liczba;
        }
    }


}
