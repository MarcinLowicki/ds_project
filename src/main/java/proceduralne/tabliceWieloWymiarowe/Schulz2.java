package proceduralne.tabliceWieloWymiarowe;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.Scanner;

public class Schulz2 {

        /*
Zad 27 - program z ćwiczeniem widzenia peryferyjnego metodą Shultza
Program ma wyświetlać tablicę 5/5 wartości.
Na środku ma być zero.
Reszta liczb losowana od 1 do 99.
Losowa z tych wartości ma być wyświetlona na górze jako poszukiwana i on zawsze musi znajdowac sie w tablicy. Program ma działać tak, że użytkownik
ma wpatrywać się w zero a jak zauważy kątem oka liczbę której ma szukać ma natychmiast wcisnąć enter.
Po 5 razach koniec programu. Na końcu pokazujemy ile czasu zajęło użytkownikowi znalezienie wszystkich liczb.
*/

    public static void main(String[] args) {

        LocalTime godzinaStartu= LocalTime.now();
        System.out.println(godzinaStartu);

        long czasPoczatkowy = System.currentTimeMillis();
        System.out.println(czasPoczatkowy);
        for (int i = 0; i < 5; i++) {
            zagrajRunde();
        }
        LocalTime godzinaZakonczenia= LocalTime.now();
        long roznicaPomiedzyGodzinami = godzinaStartu.until( godzinaZakonczenia, ChronoUnit.SECONDS );

        System.out.println(roznicaPomiedzyGodzinami + " SECONDS ");

        long czasFinalny = System.currentTimeMillis();
        long czasSekundy = (czasFinalny-czasPoczatkowy)/1000;
        System.out.println(czasSekundy + "sekundy");


    }

    static void zagrajRunde() {

        int[][] tablica5x5 = stworzTablice5x5();
        drukujPrzyjetaTablice( tablica5x5 );
        szukanaLiczbaZtablicy( tablica5x5 );
        zaczekajNaEnter();
    }

    static int[][] stworzTablice5x5() {

        int[][] tablica = new int[5][5];
        //[pięc tablic] [pięc miejsc w tablicy]
        //[ij,j,j,j,j]
        //[i,0,0,0,0]
        //[i,0,0,0,0]
        //[i,0,0,0,0]
        //[i,0,0,0,0]
        Random liczba = new Random();
        //ta petla wybiera tablice (steruje)
        for (int i = 0; i < tablica.length; i++) {
            // ta petle uzupelnia pozycje z wybranej tablicy
            for (int j = 0; j < tablica.length; j++) {
                // next int = 0 +1;
                tablica[i][j] = liczba.nextInt( 99 ) + 1;
            }
        }
        tablica[2][2] = 0;

        return tablica;
    }

    static void drukujPrzyjetaTablice(int[][] tablica) {

        for (int[] row : tablica) {
            // tablica najpierw idzie po wierszach "row" (0,0)
            for (int i : row) {
                // nastepnie idzie po kolumnach "i" kolumny
                System.out.print( i + " " );
            }
            System.out.println();
        }
    }

    static void szukanaLiczbaZtablicy(int[][] tablica) {
        Random random = new Random();
        int kolumna = random.nextInt( 5 );
        int wiersz = random.nextInt( tablica.length );

        int liczbaWylosowana = tablica[wiersz][kolumna];
        System.out.println( "Szukaj liczby: " + liczbaWylosowana );
    }

    static void zaczekajNaEnter() {
        Scanner scanner = new Scanner( System.in );
        scanner.nextLine();
    }


}
