package proceduralne.tabliceWieloWymiarowe;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GraWwisielca2 {

/*    Pierwszy gracz wymyśla słowo, ujawniając na przykład za pomocą poziomych kresek liczbę tworzących je
    liter. Drugi z graczy stara się odgadnąć litery słowa. Za każdym razem, gdy mu się to uda,
    pierwszy gracz wstawia literę w odpowiednie miejsce; w przeciwnym wypadku rysuje element
    symbolicznej szubienicy i wiszącego na niej ludzika. Jeżeli pierwszy gracz narysuje kompletnego
    „wisielca” zanim drugi odgadnie słowo, wówczas wygrywa. W zależności od wcześniej ustalonego
    stopnia skomplikowania rysunku „wisielca” (liczba elementów potrzebna do jego narysowania),
    gra pozwala na mniej lub więcej pomyłek odgadującego*/

    public static void main(String[] args) {
/*
        String litery = "";
        System.out.println("Poszukiwane litery: " + litery);


        litery += "z";
        System.out.println("Poszukiwane litery: " + litery);


        litery += "c";
        System.out.println("Poszukiwane litery: " + litery);
*/

// TODO: TABLICE WIELOWYMIAROWE

        int iloscZyc = 3
                ;

        String haslo = hasloDoZgadniecia();
        List<Character> odgadnieteLitery = new ArrayList<>();
        while (iloscZyc!=0) {

            String zbudowaneHaslo = zbudujZakodowaneHaslo( haslo, odgadnieteLitery );
            if(!zbudowaneHaslo.contains( "_" ) ){
                System.out.println("*******WYGRALES*******");
                return;
            }
            System.out.println( " Haslo do zgadniecia : " );
            System.out.println( zbudowaneHaslo );
            System.out.println( " PODAJ LITERE: " );
            Scanner podajLitere = new Scanner( System.in );
            Character litera = podajLitere.nextLine().charAt( 0 );
            boolean jestLitera = sprawdzCzyLiteraJestWhasle( haslo, litera );
            if (!jestLitera) {
                iloscZyc = iloscZyc - 1;
            }
            System.out.println( "Ilosc zycia: " + iloscZyc );
            System.out.println( "Haslo: " + haslo );

            odgadnieteLitery.add( litera );
            System.out.println( odgadnieteLitery );

        }
        System.out.println("*******Przegrales*******");

    }

    // i = 0, 12
    //     1, 11
    //     2, 10
    //     3, 9
    //     4, 8
    //     5, 7

    //     6, 6

    // komputer ustala haslo ale go nie podaje - wyswietla tyle podkreslnikow ile jest liter w hasle
    // rozpoczyna runde z konkretna iloscia prob. zakladamy 12.
    // interakcja - zgadnij litere i porownaj z haslem oraz wstaw lub odejmij zycie jesli nie zgadles
    // jesli odgadniete wszystkie - WYgrana



    static String hasloDoZgadniecia() {
        //haslo to jeden tekst/wyraz a nie tablica
        String[] haslo = {"auto", "dom", "kon"};
        Random losujZpuliHasel = new Random();
        int wylosowane = losujZpuliHasel.nextInt( 3 );
        return haslo[wylosowane];
    }
/*
    static boolean czyWygrana(String pomocniczeHaslo, List lista) {
        String obrobkaHasla = pomocniczeHaslo;
        int iloscZnakow = obrobkaHasla.length();
        for (int i = 0; i < iloscZnakow; i++) {// ala
            char aktualnaLitera = pomocniczeHaslo.charAt( i );
            if (lista.contains( aktualnaLitera )) {
            } else {
            }

        }
    }*/
//OSOBNA METODA PRZYJMIE ZBUDOWANY TEKST POSZUKA PODKLESNIKOW I JEZELI NIE ZNAJDZIE TO ZKONCZY A JAK ZNAJDZIE TO NIE ZAKANCZA
    // metoda ma zwracać zakonowane hasło
    static String zbudujZakodowaneHaslo(String pomocniczeHaslo, List lista) {
        int iloscZnakow = pomocniczeHaslo.length();
        String budowaneHaslo = "";

        for (int i = 0; i < iloscZnakow; i++) {// ala
            char aktualnaLitera = pomocniczeHaslo.charAt( i );
            if (lista.contains( aktualnaLitera )) {
                budowaneHaslo += aktualnaLitera;
            } else {
                budowaneHaslo += "_ ";

            }

            //jesli ta litera jest zwarta w obstawianych - wyswietlam litere jesli nie to wtedy gwiazdke
//           char aktualnaLitera= pomocniczeHaslo.charAt( i );
//            String jakoString = aktualnaLitera + "";
        }

        return budowaneHaslo;
    }


    static boolean sprawdzCzyLiteraJestWhasle(String haslo, Character litera) {

        System.out.println( "Sprawdzam czy litera ->" + litera + " <-jest w hasle" );
        if (haslo.contains( litera + "" )) {
            return true;
        }
        return false;
    }


}



