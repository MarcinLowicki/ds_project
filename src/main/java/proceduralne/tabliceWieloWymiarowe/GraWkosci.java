package proceduralne.tabliceWieloWymiarowe;

import java.util.Random;
import java.util.Scanner;

public class GraWkosci {

    // gra on line, link: http://www.playonlinedicegames.com/pig


    public static void main(String[] args) {
        // 1 grasz
        // 0 nie grasz
        int punktyGracz1 = 0;
        int punktyGracz2 = 0;
        String czyjaKolej = "A";

        while (punktyGracz1 <= 10 && punktyGracz2 <= 10) {

            if (czyjaKolej == "A") {
                System.out.println( "GRASZ GRACZEM A: " );
                punktyGracz1 += rundaGracza();
                czyjaKolej = "B";


            } else if (czyjaKolej == "B") {
                System.out.println( "GRASZ GRACZEM B: " );
                punktyGracz2 += rundaGracza();
                czyjaKolej = "A";

            }
            System.out.println( "Gracz A ma punktow:" + punktyGracz1 );
            System.out.println( "Gracz B ma punktow:" + punktyGracz2 );

        }
        if (czyjaKolej == "A") {
            System.out.println( " Przegral A wygral B" );
        } else {
            System.out.println( " Przegral B wygral A" );

        }

    }

    // METODA ZWRACA
    // int x ( a,b)
    // return y;

    // int x()
    //return y;

    //METODA NIC NIE ZWRACA
    //void x ();

    //void x (a,b);


    static int rzucKostka() {
        Random iloscOczek = new Random();
        int wylosowaneOczka = iloscOczek.nextInt( 6 ) + 1;
        return wylosowaneOczka;
    }

    static int czyGraszDalej() {
        System.out.println( "Czy grasz dalej kostka? Wybierz 1 lub 0" );
        Scanner wybor = new Scanner( System.in );
        int decyzja = wybor.nextInt();
        return decyzja;
    }

    static int rundaGracza() {
        int czyGraszDalej = 9;
        int sumaOczek = 0;

        while (czyGraszDalej != 0) {
            czyGraszDalej = czyGraszDalej();
            if (czyGraszDalej == 0) {
                System.out.println( "Suma Punktow graczaXY: " + sumaOczek );
                return sumaOczek;
                //return konczy metode
            }

            int wylosowalsIloscOczek = rzucKostka();
            System.out.println( "Wylosowales: " + wylosowalsIloscOczek );
            sumaOczek += wylosowalsIloscOczek;
            if (wylosowalsIloscOczek == 1) {
                sumaOczek = 0;
                System.out.println( "Nie grasz Dalej" );
                break;
                //break wychodzi z petli
            }

            System.out.println( "Suma Punktow graczaX: " + sumaOczek );
        }
        System.out.println( "Suma PunktowY: " + sumaOczek );

        return sumaOczek;
    }


}
