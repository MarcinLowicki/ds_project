package proceduralne.tabliceWieloWymiarowe;

import java.util.Random;

public class Schulz {
    /*
Zad 27 - program z ćwiczeniem widzenia peryferyjnego metodą Shultza
Program ma wyświetlać tablicę 5/5 wartości.
Na środku ma być zero.
Reszta liczb losowana od 1 do 99.
Losowa z tych wartości ma być wyświetlona na górze jako poszukiwana. Program ma działać tak, że użytkownik
ma wpatrywać się w zero a jak zauważy kątem oka liczbę której ma szukać ma natychmiast wcisnąć enter.
Po 5 razach koniec programu. Na końcu pokazujemy ile czasu zajęło użytkownikowi znalezienie wszystkich liczb.
*/
    public static void main(String[] args) {
// TODO: "dodac zero";
        int[][] tablica5x5 = new int[5][5];

        wstawLiczbeDoTablicy5x5( tablica5x5 );
        System.out.println( "------------------" );
        wyswietlTablice5x5( tablica5x5 );
    }

    static void wstawLiczbeDoTablicy5x5(int[][] tab5x5) {
        Random randomLiczba = new Random();
        for (int i = 0; i < tab5x5.length; i++) {
            for (int j = 0; j < tab5x5.length; j++) {
                tab5x5[i][j] = randomLiczba.nextInt( 99 );
            }
        }
    }

    static void wyswietlTablice5x5(int[][] tab5x5) {
        for (int[] wierszZwielomaLiczbami : tab5x5) {
            for (int liczbaZwiersza : wierszZwielomaLiczbami) {
                System.out.print( liczbaZwiersza + " " );

            }
            System.out.println( " " );
        }

    }

}
