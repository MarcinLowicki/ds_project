package proceduralne.stacjePaliw;

import java.util.Scanner;

public class StacjaPaliw {


    public static void main(String[] args) {


        // 3 stacje - przechowuje informacje cene paliw(1 stacja 1 rodzaj paliwa)
        // uzytkownik moze wybrac na ktorej stacji tankuje
        // ile paliwa tankuje
        // moze powtarzac te operacje dopoki nie zakonczy
        //na zakonczenie program wyswietla ile zl wydal na kazdej ze stacji


        String[] nazwaStacji = {"A", "B", "C"};
        int[] zatankowaneLitry = {0, 0, 0};
        double[] cenyPaliw = {5.50, 5.20, 2.60};
        int wybranyNumerStacji;

        do {
            wyswietlDostepneStacje( nazwaStacji );
            wybranyNumerStacji = wybierzNumerStacji(); //0
            if (wybranyNumerStacji == 0) {
                break;
            }
            zatankowaneLitry[wybranyNumerStacji - 1] = zatankowanaIloscLitrow();
            System.out.println( "Zatankowales litrow " + zatankowaneLitry[wybranyNumerStacji - 1] );
            podsumowanieTankowania( nazwaStacji, zatankowaneLitry, cenyPaliw );


        } while (wybranyNumerStacji != 0);
        System.out.println( "Zakończyłeś tankowanie" );
        podsumowanieTankowania(nazwaStacji, zatankowaneLitry, cenyPaliw );
    }


    static void wyswietlDostepneStacje(String[] nazwaStacji) {
        for (int i = 0; i < nazwaStacji.length; i++) {
            System.out.println( i + 1 + ". " + nazwaStacji[i] );

        }
        System.out.println( "0. zakończ" );
    }

    static int wybierzNumerStacji() {
        System.out.println( "Na jakim Numerze stacji tankujesz?" );
        Scanner scanner = new Scanner( System.in );
        int wybranaStacja = scanner.nextInt();
        System.out.println( "Wybrałeś stację " + wybranaStacja );
        return wybranaStacja;
    }

    static int zatankowanaIloscLitrow() {
        System.out.println( "Ile litrow zatankowales?" );
        Scanner scanner = new Scanner( System.in );
        int iloscLitrow = scanner.nextInt();
        System.out.println( "Wybrałeś stację " + iloscLitrow );
        return iloscLitrow;
    }

    static void podsumowanieTankowania(String[] nazwaStacji, int[] zatankowaneLitry, double[] cenyPaliw) {

        for (int i = 0; i < nazwaStacji.length; i++) {
            System.out.println( "Nazwa Stacji: " + nazwaStacji[i] );
            System.out.println( "Cena paliw: " + cenyPaliw[i] );
            System.out.println( "Zatankowane litry: " + zatankowaneLitry[i] );
            System.out.println("WYDAŁEŚ : " + zatankowaneLitry[i] * cenyPaliw[i] + " PLN");
        }
        System.out.println( "***********PODSUMOWANIE************" );
    }




}
