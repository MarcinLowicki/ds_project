package proceduralne.asystent;

import fundamenty.loteria.SymulatorLoterii;

import java.util.Random;
import java.util.Scanner;

public class Asystent {
    //Projekt 4: Asystent
    static String[] opcjeMenu = {"Loteria", "Sklep AGD", "Fundamenty", "Zakoncz Program"};

    public static void main(String[] args) {
        rozpocznijProgram();
    }

    public static void rozpocznijProgram() {
        System.out.println( "Asystent mowi: " + generujPowitanie2() );
        uruchomMenu();
        System.out.println( "Asystent pozegnania mowi: " + generujPozegnanie() );
    }

    public static void uruchomMenu() {
        int jednaLiczba;
        do {
            drukujListeOpcji();
            jednaLiczba = pobierzPozycje();
            uruchomOpcje( jednaLiczba );
        } while (jednaLiczba != 4);
    }

    public static int pobierzPozycje() {
        Scanner scanner = new Scanner( System.in );
        System.out.println( "Pobierz pozycje z Menu: " );
        int x = scanner.nextInt();
        return x;
    }


    public static void drukujListeOpcji() {

        System.out.println( "MENU:" );
        for (int i = 0; i < opcjeMenu.length; i++) {
            String opcje = opcjeMenu[i];
            System.out.println( (i + 1) + " " + opcje );

        }
    }

    public static void uruchomOpcje(int i) {
        switch (i) {

            case 1:
                System.out.println( "Uruchamiasz Loterie" );
                SymulatorLoterii.uruchomLoterie();
                break;

            case 2:
                System.out.println( "Sklep AGD" );
                break;

            case 3:
                System.out.println( "Fundamenty" );
                break;

            case 4:
                System.out.println( "Zakoncz Program" );
                break;
            default:
                System.out.println( "Nic nie wybralem" );
        }


    }


    public static void generujPowitanie() {
        Random randomLosowanie = new Random();
        int wylosowanyNumer = randomLosowanie.nextInt( 5 );
        System.out.println( wylosowanyNumer );
        String zmiennaPrzywitanie;
        switch (wylosowanyNumer) {
            case 0:
                zmiennaPrzywitanie = "Hallo";
                break;
            case 1:
                zmiennaPrzywitanie = "Hej";
                break;
            case 2:
                zmiennaPrzywitanie = "Hi";
                break;
            case 3:
                zmiennaPrzywitanie = "Salutto";
                break;
            case 4:
                zmiennaPrzywitanie = "Czesc";
                break;
            default:
                zmiennaPrzywitanie = "brak przywitania";
        }
        System.out.println( zmiennaPrzywitanie );
    }

    public static String generujPowitanie2() {
        String[] tablicaPrzywitan = {
                "Siema", "Hi", "Servus", "Dzien Dobry", "HelllloL", "Bryyy"};
        Random randomLosowanie = new Random();
        int wylosowanyNumer = randomLosowanie.nextInt( tablicaPrzywitan.length );
        //System.out.println( wylosowanyNumer );


        return tablicaPrzywitan[wylosowanyNumer];
    }

    public static String generujPozegnanie() {
        Random randomLosowanie = new Random();
        String zmiennaPa = "";
        int wylosowanyNumer = randomLosowanie.nextInt( 5 ) + 1;
        // System.out.println( wylosowanyNumer );
        for (int i = 0; i < wylosowanyNumer; i++) {
            zmiennaPa += "pa";
        }
        return zmiennaPa + "!";
    }


}
