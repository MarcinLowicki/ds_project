package obiektowe.kompozycja.garden;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TreeTest {

    @Test
    void giveFruitsShouldIncreaseNumberOfFruitsOnTree() {
        Tree tree = new Tree();
        int startingNumerOfFruits = tree.getNumerOfFruits();
        tree.givesFruits();
        Assertions.assertTrue( startingNumerOfFruits < tree.getNumerOfFruits() );
    }

    @Test
    void collectFruitsShouldTakeAllOfTheFruits(){
        Tree tree = new Tree();
        int startingNumerOfFruits = tree.getNumerOfFruits();
        int howManyFruitsHaveWeCollect = tree.collectFruit();

        Assertions.assertEquals( startingNumerOfFruits,howManyFruitsHaveWeCollect );
        Assertions.assertEquals( 0, tree.getNumerOfFruits() );
    }

}