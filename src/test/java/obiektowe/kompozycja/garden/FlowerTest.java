package obiektowe.kompozycja.garden;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlowerTest {

    @Test
    void watheringFlowerShouldMakeIiBloom() {

        Flower flower = new Flower();
        flower.waterTheFlower();
        Assertions.assertEquals( true, flower.isBlooms() );
    }

    @Test
    void newFlouerDontBloom(){

        Flower flower = new Flower(  );
        Assertions.assertFalse( flower.isBlooms() );
    }

}